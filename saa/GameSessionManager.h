//
//  GameSessionManager.h
//  saa
//
//  Created by Dennis Park on 8/7/12.
//  Copyright Reliable Source, LLC 2012. All rights reserved.
//

#import <Foundation/Foundation.h>
#import "GameConfig.h"

@interface GameSessionManager : NSObject{
    int _score;
    int _correct;
    int _incorrect;
    int _currentProblem;
    int _mode;
    int _hintState;
}
+(GameSessionManager *)sharedGameSessionManagerInstance;
-(void)resetSession;
-(int)getScore;
@property (nonatomic) int score;
@property (nonatomic) int correct;
@property (nonatomic) int incorrect;
@property (nonatomic) int currentProblem;
@property (nonatomic) int mode;
@property (nonatomic) int hintState;


@end
