//
//  LayerGameAddition.m
//  Kinopi
//
//  Created by  Dennis Park (dennis.park@gmail.com) on 5/22/12.
//              Phill Pafford (phill.pafford@gmail.com)
//  Copyright Reliable Source LLC 2012. All rights reserved.
//


// Import the interfaces
#import "LayerGameAddition.h"
#import "SimpleAudioEngine.h"
#import "CDAudioManager.h"
#import "CDOpenALSupport.h"
#import "CDConfig.h"
#import "GameSessionManager.h"
#import "SimpleAudioEngine.h"
#import "SceneFactory.h"

// LayerGameAddition implementation
@implementation LayerGameAddition;

@synthesize zero    = zero_;
@synthesize one     = one_;
@synthesize two     = two_;
@synthesize three   = three_;
@synthesize four    = four_;
@synthesize five    = five_;
@synthesize six     = six_;
@synthesize seven   = seven_;
@synthesize eight   = eight_;
@synthesize nine    = nine_;

@synthesize numberValues = numberValues_;

@synthesize placeSystemValues;


@synthesize positionWithValueCarry;
@synthesize positionWithValueAugend;
@synthesize positionWithValueAddend;
@synthesize positionWithValueSum;

@synthesize activeSprite = activeSprite_;
@synthesize numberMenuTotalSpriteCount;
@synthesize equationNumberCountAugend;
@synthesize equationNumberCountAddend;
@synthesize equationNumberTotalCount;

// END REFACTOR

@synthesize carryPositions;
@synthesize sumPositions;

@synthesize bucketAugend;
@synthesize bucketAddend;

@synthesize equationLine_y = equationLine_y_;
@synthesize equationLine_x_start = equationLine_x_start_;
@synthesize equationLine_x_end = equationLine_x_end_;

@synthesize screenWidth = screenWidth_;
@synthesize screenHeight = screenHeight_;
@synthesize inputBackgroundDim = inputBackgroundDim_;

-(id) init
{
	if( (self=[super init])) {
        int gameMode = [GameSessionManager sharedGameSessionManagerInstance].mode;
        
        NSLog(@"Mode is:  %d", gameMode);
        
        // create and initialize a Label
		// ask director the the window size
		CGSize size = [[CCDirector sharedDirector] winSize];
        
        screenHeight_ = size.height;
        screenWidth_  = size.width;
        
        NSLog(@"screenheight:  @%f", screenHeight_);
        NSLog(@"screenwidth:  @%f", screenWidth_);
        
        if(UI_USER_INTERFACE_IDIOM() == UIUserInterfaceIdiomPad){
            screenScaleFactor_x_ = 1;
            screenScaleFactor_y_ = 1;
        }else{
            screenScaleFactor_x_ = 1024.0f/screenHeight_;
            screenScaleFactor_y_ = 768.0f/screenWidth_;
            
            NSLog(@"screenscale factor x:  @%f", screenScaleFactor_x_);
            NSLog(@"screenscale factor y:  @%f", screenScaleFactor_y_);
        }
        
        numberMenuLayoutOrientation_ = LAYOUT_RHS;
        numberMenuLayoutColumns_ = LAYOUT_ONE_COL;
        
        // @TODO: Use Correct Terminology as Variable Decalration
        //        This will make it easier to add additional modules
        //        such as Subtraction, Division and Multiplication
        // http://mathforum.org/library/drmath/view/58398.html
        // http://math.stackexchange.com/questions/152699/what-are-the-carried-numbers-called-in-an-addition-problem
        // 
        // Porto:   The number(s) Carried - Porto is the Latin word for Carry
        // Augend:  The first number in a Addition Problem
        // Addend:  The Number(s) being added to the Augend in a Addition Problem
        // Sum:     The result of adding the Augend and Addend numbers
        //
        // Columnar Addiiton Grid Layout
        // http://www.calculla.com/en/columnar_add
        //
        // Carry:           One row to hold the Carry
        // Given Numbers:   Minimum of two rows in this grid, one for the Augend and additional rows for the Addend
        // Result:          One row to hold the Sum
        //
        // Multidimensional NSMutableArray
        // http://stackoverflow.com/questions/4982617/objective-c-create-a-multi-dimensional-array-with-the-dimensions-specified-at
        //
        // Clone / Duplicate
        // http://stackoverflow.com/questions/4543096/making-a-duplicate-of-a-ccsprite
        //
        // Grid Layout Ideas
        // http://www.cocos2d-iphone.org/forum/topic/1168
        
        // LOGIC NUGGET
        // Formula to find how many numbers need to be available in each bucket
        // Find the longest number, either the augend or addend + 1
        // Example
        // 99 + 99 = 198
        // so 3 digits should be the max allowd in each bucket
        
        // START REFACTOR
        activeSprite_               = nil;   //  id of activeSprite
        numberMenuTotalSpriteCount  = 10;   // 0 - 9 number menu sprites
        
        // Add the numbers for the equation first
        // Top number of the Equation: Augend
        // NOTE: Augend should be the longer of the two numbers
        equationNumberCountAugend   = CONFIG_SESSION_EQUATION_NUMBER_COUNT_AUGEND;
        equationNumberCountAddend   = CONFIG_SESSION_EQUATION_NUMBER_COUNT_ADDEND;
        equationNumberTotalCount    = ( equationNumberCountAugend + equationNumberCountAddend );
        
        // Integer Values
        zero_   = [[NSNumber alloc] initWithInteger:0];
        one_    = [[NSNumber alloc] initWithInteger:1];
        two_    = [[NSNumber alloc] initWithInteger:2];
        three_  = [[NSNumber alloc] initWithInteger:3];
        four_   = [[NSNumber alloc] initWithInteger:4];
        five_   = [[NSNumber alloc] initWithInteger:5];
        six_    = [[NSNumber alloc] initWithInteger:6];
        seven_  = [[NSNumber alloc] initWithInteger:7];
        eight_  = [[NSNumber alloc] initWithInteger:8];
        nine_   = [[NSNumber alloc] initWithInteger:9];
        
        // Push the NSNumbers into this array
        numberValues_= [NSArray arrayWithObjects:zero_, one_, two_, three_, four_, five_, six_, seven_, eight_, nine_, nil];
        [numberValues_ retain];
                
        // Place Systems
        // The position the number is in
        placeSystemValues   = [[[NSArray alloc] initWithObjects: [NSNumber numberWithInt:1], [NSNumber numberWithInt:10], [NSNumber numberWithInt:100], [NSNumber numberWithInt:1000], [NSNumber numberWithInt:10000], nil] autorelease];
        
        [placeSystemValues retain];
                        
        positionWithValueCarry     = [[NSMutableDictionary alloc] init];
        positionWithValueAugend    = [[NSMutableDictionary alloc] init];
        positionWithValueAddend    = [[NSMutableDictionary alloc] init];
        positionWithValueSum       = [[NSMutableDictionary alloc] init];
        
        
        // We need to add zero at the tens position for the positionWithValueCarry Dictionary
        // @TODO: How do we get the placeSystemValues from the array? or is this overkill?
        //[positionWithValueCarry setValue:zero_ forKey:[NSString stringWithFormat:@"%d", [NSNumber numberWithInt:1]]];
        [positionWithValueCarry setValue:zero_ forKey:[placeSystemValues objectAtIndex:1]];
                
        // Buckets
        bucketAugend = [[NSMutableArray alloc] init];
        bucketAddend = [[NSMutableArray alloc] init];
        
        carryPositions  = [[NSMutableArray alloc] init];
        sumPositions    = [[NSMutableArray alloc] init];
        
        clonedNumbers_      = [[NSMutableArray alloc] init];
        numbersMenuItems_   = [[NSMutableArray alloc] init];
        
        CGFloat randomNumberWidth;
        CGFloat randomNumberHeight;
                
        fontSize_ = (DIM_FONT / ((screenScaleFactor_x_ + screenScaleFactor_y_)/2));
        lineWidth_ = fontSize_ /12;
        
        CCLabelTTF *randomNumber = [CCLabelTTF labelWithString:[NSString stringWithFormat:@"%d", 8] fontName:@"Marker Felt" fontSize:fontSize_];
        randomNumberWidth     = [randomNumber boundingBox].size.width;
        randomNumberHeight    = [randomNumber boundingBox].size.height;
        
        NSLog(@"randomNumberWidth %f", randomNumberWidth);
        NSLog(@"randomNumberHeight %f", randomNumberHeight);
        
        inputBackgroundDim_ = CGRectMake(0,0,[randomNumber boundingBox].size.width,[randomNumber boundingBox].size.height);
        NSLog(@"--->input background dim:  %f, %f",inputBackgroundDim_.size.width, inputBackgroundDim_.size.height);
                
        [self clearData];
        [self generateMathProblem];
        
        CCLabelTTF *operatorPlus = [CCLabelTTF labelWithString:@"+" fontName:@"Marker Felt" fontSize:fontSize_];
        CCLabelTTF *scoreText = [CCLabelTTF labelWithString:LABEL_SCORE fontName:@"Marker Felt" fontSize:fontSize_];
        CCLabelTTF *numCorrect = [CCLabelTTF labelWithString:@"0" fontName:@"Marker Felt" fontSize:fontSize_];
        CCLabelTTF *div = [CCLabelTTF labelWithString:@" / " fontName:@"Marker Felt" fontSize:fontSize_];
        CCLabelTTF *total = [CCLabelTTF labelWithString:@"0" fontName:@"Marker Felt" fontSize:fontSize_];
        
        CGFloat scoreWidth = [scoreText boundingBox].size.width;
        CGFloat scoreHeight = [scoreText boundingBox].size.height;
        
        CGFloat divWidth = [div boundingBox].size.width;
        CGFloat numCorrectWidth = [numCorrect boundingBox].size.width;        
        CGFloat totalWidth = [total boundingBox].size.width;
        
        scoreText.position = ccp((scoreWidth / 2), self.screenHeight - (scoreHeight / 2));
        numCorrect.position = ccp(((scoreWidth) + (numCorrectWidth / 2)), self.screenHeight - (scoreHeight / 2));
        div.position = ccp(((scoreWidth) + (numCorrectWidth) + (divWidth/2)), self.screenHeight - (scoreHeight / 2));

        total.position = ccp(((scoreWidth) + (numCorrectWidth) + (divWidth) + (totalWidth/2) ), self.screenHeight - (scoreHeight / 2));
        
        if (gameMode == CONFIG_SESSION_MODE_QUIZ)
        {
            [self addChild:scoreText];
            [self addChild:numCorrect z:1 tag:DISPLAY_CORRECT];
            [self addChild:div];
            [self addChild:total z:1 tag:DISPLAY_TOTAL];
        }
            
        // Plus Operator
        CGFloat plusWidth     = [operatorPlus boundingBox].size.width;
        CGFloat plusHeight    = [operatorPlus boundingBox].size.height;
        
        operatorPlus.position =  ccp( self.screenWidth /2 + - (plusWidth * 2), self.screenHeight/2 - plusHeight );
        
       // @TODO: create plus sprite
        
        // add sprites to scene layer.
        [self addChild:operatorPlus  z:1];

        [self createActionMenu:gameMode];
        
        float spriteHeight  = 0;
        float spriteWidth   = 0;
                
        // Add the carry boundaries, this should be the same as the equationNumberCountAugend
        for(int i = 0; i < equationNumberCountAugend; i++) {
            CCLabelTTF *carryPlaceholder = [CCLabelTTF labelWithString:[NSString stringWithFormat:@" "] fontName:@"Marker Felt" fontSize:fontSize_];
            
            CCSprite * spriteCarryObj = [CCSprite spriteWithFile:IMAGE_BLANK_TILE_67_X_67]; // rect: inputBackgroundDim_];
            //CCSprite * spriteCarryObj = [CCSprite spriteWithFile:@"blank.png" rect: inputBackgroundDim_]; //

            [spriteCarryObj setColor:ccBLUE];
            [self addChild:spriteCarryObj z:0];
            
            spriteHeight  = [spriteCarryObj boundingBox].size.height;
            spriteWidth   = [spriteCarryObj boundingBox].size.width;
            [spriteCarryObj addChild:carryPlaceholder z: 1];            
            [carryPlaceholder setPosition:ccp(spriteWidth/2, spriteHeight/2)];
            
            
            // Build the Carry above the Augend First ( Top Level )
            if(i <= equationNumberCountAugend) {
                // The carry values should all be zero but we still need the Place System Values
                [positionWithValueCarry setValue:zero_ forKey:[placeSystemValues objectAtIndex:(equationNumberCountAugend - i)]];
                
                spriteCarryObj.position = ccp(self.screenWidth / 2 + (spriteWidth * (i - 1)), self.screenHeight / 2 + spriteHeight);
            } 

            NSString *_place = [placeSystemValues objectAtIndex:(equationNumberCountAugend - i)];
            [carryPlaceholder setUserData:_place];
            [carryPositions addObject:spriteCarryObj];
        }

        // Add the SUM boundaries, this should be one more than the equationNumberCountAugend
        for(int i = 0; i < (equationNumberCountAugend + 1); i++) {
            CCLabelTTF *sumPlaceholder = [CCLabelTTF labelWithString:[NSString stringWithFormat:@" "] fontName:@"Marker Felt" fontSize:fontSize_];

            CCSprite * spriteSumObj = [CCSprite spriteWithFile:IMAGE_BLANK_TILE_67_X_67]; // rect:inputBackgroundDim_];
            //CCSprite * spriteSumObj = [CCSprite spriteWithFile:@"blank.png" rect:inputBackgroundDim_]; //
            
            [spriteSumObj setColor:ccBLUE];
            [self addChild:spriteSumObj z:0];
            
            float spriteHeight  = [spriteSumObj boundingBox].size.height;
            float spriteWidth   = [spriteSumObj boundingBox].size.width;
            [spriteSumObj addChild:sumPlaceholder z: 1];            
            [sumPlaceholder setPosition:ccp(spriteWidth/2, spriteHeight/2)];
            
            // Build the Augend First ( Top Level )
            if(i <= equationNumberCountAugend) {
                // The carry values should all be zero but we still need the Place System Values
                [positionWithValueSum setValue:zero_ forKey:[placeSystemValues objectAtIndex:(equationNumberCountAugend - i)]];
                
                spriteSumObj.position = ccp(self.screenWidth / 2 + (spriteWidth * (i - 1)), (self.screenHeight / 2 - spriteHeight * 2) - 10);
                // @TODO: Should account for the widht of the equation line, using 10 for now
            } 

            NSString *_place = [placeSystemValues objectAtIndex:(equationNumberCountAugend - i)];
            [sumPlaceholder setUserData:_place];
            [sumPositions addObject:spriteSumObj];
        }
        
        // set the equation line parameters to be used in the draw method.
        // screen scale factors are flipped.
        
        NSLog(@"*****fontsize:  @%d", fontSize_);
        
        equationLine_y_ = (self.screenHeight / 2) - ((spriteHeight*2) - (((fontSize_/2))/screenScaleFactor_y_));  // 30 is a MAGIC NUMBER
        equationLine_x_start_ = (self.screenWidth /2) + - ((plusWidth * 2) + (25/screenScaleFactor_x_));   // 25 is a MAGIC NUMBER
        equationLine_x_end_ = (self.screenWidth /2) + - (plusWidth * 2) + (spriteWidth * (equationNumberCountAugend + 1)) + (3/screenScaleFactor_x_);  // 3 is a MAGIC NUMBER
        
        // Build the Sprites Menu 0 - 9
        for(int i = 1; i <= numberMenuTotalSpriteCount; i++) {
            numberMenulabelPlaceholder = i - 1;
            CCLabelTTF *number = [CCLabelTTF labelWithString:[NSString stringWithFormat:@"%d", numberMenulabelPlaceholder] fontName:@"Marker Felt" fontSize:fontSize_];
                        
            CCSprite *spriteObj = [CCSprite spriteWithFile:IMAGE_BLANK_TILE_67_X_67 ];// rect:inputBackgroundDim_];
            //CCSprite *spriteObj = [CCSprite spriteWithFile:@"button_background.png" rect:inputBackgroundDim_];
            float spriteHeight  = [spriteObj boundingBox].size.height;
            float spriteWidth   = [spriteObj boundingBox].size.width;
            
            [self addChild:spriteObj z:0];

            [number setPosition:ccp(spriteWidth/2, spriteHeight/2)];
            [spriteObj addChild:number z: 1];
            [self setNumberPositionOnMenu:spriteObj];
            [numbersMenuItems_ addObject:spriteObj];
            
            CCRotateBy *rot_fwd = [CCRotateBy actionWithDuration:0.15f angle:+4.0f];
            CCRotateBy *rot_back = [CCRotateBy actionWithDuration:0.3f angle:-8.0f];
            CCRepeatForever *shake = [CCRepeatForever actionWithAction:[CCSequence actionsWithArray:[NSArray arrayWithObjects:rot_fwd, rot_back, rot_fwd, nil]]];
            [number runAction:shake];
        }
        
        [self logAppState];
		self.isTouchEnabled = YES;
        
        if (gameMode == CONFIG_SESSION_MODE_PRACTICE)
        {
            [self performSelector:@selector(giveHint:)];
        }
	}
	return self;
}

-(void)clearMathProblem{
    NSEnumerator *it = [bucketAddend objectEnumerator];
    id obj = nil;
    while (obj = [it nextObject]) {
        [self removeChild:obj cleanup:YES];
    }
    
    it = [bucketAugend objectEnumerator];
    while (obj = [it nextObject]) {
        [self removeChild:obj cleanup:YES];
    }
    
    [bucketAddend removeAllObjects];
    [bucketAugend removeAllObjects];
    
}

-(void)clearData{
    // remove from self first.
    NSEnumerator *it = [clonedNumbers_ objectEnumerator];
    id obj = nil;

    it = [clonedNumbers_ objectEnumerator];
    while (obj = [it nextObject]) {
        [self removeChild:obj cleanup:YES];
    }
    
    it = [positionWithValueAugend objectEnumerator];
    while (obj = [it nextObject]) {
        [self removeChild:obj cleanup:YES];
    }
    
    it = [positionWithValueAddend objectEnumerator];
    while (obj = [it nextObject]) {
        [self removeChild:obj cleanup:YES];
    }
    
    it = [positionWithValueCarry objectEnumerator];
    while (obj = [it nextObject]) {
        [self removeChild:obj cleanup:YES];
    }
    
    it = [positionWithValueSum objectEnumerator];
    while (obj = [it nextObject]) {
        [self removeChild:obj cleanup:YES];
    }
    
    
    [positionWithValueAugend removeAllObjects];
    [positionWithValueAddend removeAllObjects];
    [positionWithValueCarry removeAllObjects];
    [positionWithValueSum removeAllObjects];
    [clonedNumbers_ removeAllObjects];
    
    previousHoveredOverSprite_ = nil;
    hoveredOverSprite_ = nil;
    activeSprite_ = nil;
    currentlySelectedNumber_ = nil;
}

-(void)resetProblem:(id)sender{
    NSLog(@"reset pressed");
    [self _reset];
    [self _clearPlaceHoldersPreview];
    [self resetInputScales];
    [self resetInputColors];
    
    GameSessionManager *gm = [GameSessionManager sharedGameSessionManagerInstance];

    if (gm.hintState == CONFIG_SESSION_HINT_MODE_ON && gm.mode == CONFIG_SESSION_MODE_PRACTICE)
    {
        [self performSelector:@selector(giveHint:)];
    }
}

-(void)nextProblem:(id)sender {
    NSLog(@"Next pressed");

    GameSessionManager *gm = [GameSessionManager sharedGameSessionManagerInstance];
    
    int selectedMode = gm.mode;
    NSLog(@"Selected Mode: %d", selectedMode);
    
    if (selectedMode == CONFIG_SESSION_MODE_PRACTICE)
    {
        [self clearData];
        [self clearMathProblem];
        [self generateMathProblem];
        [self resetInputScales];
        [self resetInputColors];
        [self performSelector:@selector(giveHint:)];
    }
    else if (selectedMode == CONFIG_SESSION_MODE_QUIZ)
    {
        /*
         *      This checks to see if the answer is correct
         *      (by calling isAnswerCorrect).  If the answer is correct the this method
         *      will  1) increment the score by 1.  2)Check to see if the session if
         *      complete.  If so, then the LevelComplete scene will be loaded.  If the
         *      session is not complete then the next problem is loaded and the current
         *      game state is reset.
         */
        if([self isAnswerCorrect]){
            NSLog(@"Correct.");
            
            CCLabelTTF *_score = ((CCLabelTTF*)[self getChildByTag:DISPLAY_CORRECT]);
            GameSessionManager *gm = [GameSessionManager sharedGameSessionManagerInstance];
            gm.correct++;
            
            NSNumber *n = [[NSNumber alloc] initWithInteger:gm.correct];
            
            [_score setString:n.stringValue];

        }else{
            NSLog(@"Incorrect.");
        }

        gm.currentProblem++;
        
        if(gm.currentProblem >= CONFIG_SESSION_TOTAL_PROBLEMS){
            [self showLevelComplete];
        }
        
        CCLabelTTF *_total = ((CCLabelTTF*)[self getChildByTag:DISPLAY_TOTAL]);
        NSNumber *n = [[NSNumber alloc] initWithInteger:gm.currentProblem];
        [_total setString:n.stringValue];
        
        [self clearData];
        [self clearMathProblem];
        [self generateMathProblem];
        [self resetInputScales];
        [self resetInputColors];

    }
}

-(void)showLevelComplete{
    CCScene * sceneLevelComplete = [[SceneFactory sharedSceneFactoryInstance] getScene:@"sceneLevelComplete"];
    [[CCDirector sharedDirector] pushScene:sceneLevelComplete];
    
    CCLabelTTF *_score = ((CCLabelTTF*)[self getChildByTag:DISPLAY_CORRECT]);
    [_score setString:@"0"];
}

-(void)giveHint:(id)sender{
    NSLog(@"give hint pressed");
    
    GameSessionManager *gm = [GameSessionManager sharedGameSessionManagerInstance];
    
    if (gm.hintState == CONFIG_SESSION_HINT_MODE_ON && gm.mode == CONFIG_SESSION_MODE_PRACTICE)
    {
        NSMutableArray * hintSpriteArray = [self getHint];
        
        if([hintSpriteArray count] > 0){
            
            @try{
                // index one will be value
                CCSprite * hintSpriteValue = [hintSpriteArray objectAtIndex:1];
                CCLabelTTF * hintSpriteValueTTF = (CCLabelTTF*)[[hintSpriteValue children] lastObject];
                [hintSpriteValueTTF setColor:ccGREEN];
            }
            @catch (NSException * e) {
                NSLog(@"Exception in hint value: %@", e);
            }
            @finally{}
            
            @try{
                // index zero is position
                CCSprite * hintSpritePosition = [hintSpriteArray objectAtIndex:0];
                CCLabelTTF * hintSpritePositionTTF = (CCLabelTTF*)[[hintSpritePosition children] lastObject];
                [hintSpritePositionTTF setColor:ccGREEN];
            }
            @catch (NSException * e) {
                NSLog(@"Exception in hint position: %@", e);

            }
            @finally{}     
        }
    }
}

-(void)toggleHintState:(id)sender{
    NSLog(@"Hint State Changed");
    
    GameSessionManager *gm = [GameSessionManager sharedGameSessionManagerInstance];
    
    CCMenu * _hint_toggle_state = ((CCMenu*)[self getChildByTag:HINT_TOGGLE_STATE]);
    CCMenuItemFont * _hint_toggle_menu_item = ((CCMenuItemFont*)_hint_toggle_state.children.lastObject);
    
    NSString* _hint_toggle_state_value = _hint_toggle_menu_item.label.string;
    
    // @todo: refactor to check gm but also apdate the label
    if ([_hint_toggle_state_value isEqualToString:LABEL_HINT_ON])
    {
        [_hint_toggle_menu_item setString:LABEL_HINT_OFF];
        gm.hintState = CONFIG_SESSION_HINT_MODE_OFF;
        [self resetInputColors];
    }
    else if ([_hint_toggle_state_value isEqualToString:LABEL_HINT_OFF])
    {
        [_hint_toggle_menu_item setString:LABEL_HINT_ON];
        gm.hintState = CONFIG_SESSION_HINT_MODE_ON;
        [self performSelector:@selector(giveHint:)];
    }
    else
    {
        NSLog(@"Invalid Hint Toggle State");
    }
}

-(void)goHome:(id)sender{
    NSLog(@"go home pressed.");
    [[CCDirector sharedDirector] popScene];
}

-(void)createActionMenu:(int)gameMode{
    
    //Options Label
    CCMenuItemFont *menuItemReset = [CCMenuItemFont itemFromString:LABEL_RESET target:self selector:@selector(resetProblem:)];
    CCMenuItemFont *menuItemNext = [CCMenuItemFont itemFromString:LABEL_NEXT target:self selector:@selector(nextProblem:)];
    CCMenuItemFont *menuItemHint = [CCMenuItemFont itemFromString:LABEL_HINT_ON target:self selector:@selector(toggleHintState:)];
    CCMenuItemFont *menuItemHome = [CCMenuItemFont itemFromString:LABEL_HOME target:self selector:@selector(goHome:)];
    
    CCMenu *menuReset = [CCMenu menuWithItems: menuItemReset, nil];
    CCMenu *menuNext = [CCMenu menuWithItems: menuItemNext, nil];
    CCMenu *menuHint = [CCMenu menuWithItems: menuItemHint, nil];
    CCMenu *menuHome = [CCMenu menuWithItems: menuItemHome, nil];
    
    if (gameMode == CONFIG_SESSION_MODE_PRACTICE)
    {
        [self addChild:menuReset z:999];
        [self addChild:menuNext z:999];
        [self addChild:menuHint z:999 tag:HINT_TOGGLE_STATE];
        [self addChild:menuHome z:999];
        
        menuNext.position =  ccp( ([menuItemNext boundingBox].size.width / 2) + (PADDING_HORIZONTAL),self.screenHeight - (self.screenHeight - [menuItemNext boundingBox].size.height) + self.screenHeight - (self.screenHeight - [menuItemHome boundingBox].size.height));
        
        menuReset.position = ccp( ([menuItemReset boundingBox].size.width / 2) +  (PADDING_HORIZONTAL),self.screenHeight - (self.screenHeight - [menuItemReset boundingBox].size.height) + self.screenHeight - (self.screenHeight - [menuItemNext boundingBox].size.height) + self.screenHeight - (self.screenHeight - [menuItemHome boundingBox].size.height));
        
        menuHint.position = ccp( ([menuItemHint boundingBox].size.width / 2) +  (PADDING_HORIZONTAL),self.screenHeight - (self.screenHeight - [menuItemReset boundingBox].size.height) + self.screenHeight - (self.screenHeight - [menuItemNext boundingBox].size.height) + self.screenHeight - (self.screenHeight - [menuItemHome boundingBox].size.height) + self.screenHeight - (self.screenHeight - [menuItemHint boundingBox].size.height));
        
        menuHome.position = ccp( ([menuItemHome boundingBox].size.width / 2) +  (PADDING_HORIZONTAL),self.screenHeight - (self.screenHeight - [menuItemReset boundingBox].size.height) + self.screenHeight - (self.screenHeight - [menuItemNext boundingBox].size.height) + self.screenHeight - (self.screenHeight - [menuItemHome boundingBox].size.height) + self.screenHeight - (self.screenHeight - [menuItemHint boundingBox].size.height) + + self.screenHeight - (self.screenHeight - [menuItemHome boundingBox].size.height) + self.screenHeight - (self.screenHeight - [menuItemHome boundingBox].size.height));
    }
    else if (gameMode == CONFIG_SESSION_MODE_QUIZ)
    {
        [self addChild:menuReset z:999];
        [self addChild:menuNext z:999];
        [self addChild:menuHome z:999];
        
        menuNext.position =  ccp( ([menuItemNext boundingBox].size.width / 2) + (PADDING_HORIZONTAL),self.screenHeight - (self.screenHeight - [menuItemNext boundingBox].size.height) + self.screenHeight - (self.screenHeight - [menuItemHome boundingBox].size.height));
        
        menuReset.position = ccp( ([menuItemReset boundingBox].size.width / 2) +  (PADDING_HORIZONTAL),self.screenHeight - (self.screenHeight - [menuItemReset boundingBox].size.height) + self.screenHeight - (self.screenHeight - [menuItemNext boundingBox].size.height) + self.screenHeight - (self.screenHeight - [menuItemHome boundingBox].size.height));
        
        menuHome.position = ccp( ([menuItemHome boundingBox].size.width / 2) +  (PADDING_HORIZONTAL),self.screenHeight - (self.screenHeight - [menuItemReset boundingBox].size.height) + self.screenHeight - (self.screenHeight - [menuItemNext boundingBox].size.height) + self.screenHeight - (self.screenHeight - [menuItemHome boundingBox].size.height) + self.screenHeight - (self.screenHeight - [menuItemHint boundingBox].size.height) + + self.screenHeight - (self.screenHeight - [menuItemHome boundingBox].size.height) + self.screenHeight - (self.screenHeight - [menuItemHome boundingBox].size.height));
    }
    else
    {
        NSLog(@"Invalid Mode passed");
    }
    
}

-(void)generateMathProblem{
    CCSprite *randomNumberSprite;
    // Build the Random Number Sprites
    for(int i = 1; i <= equationNumberTotalCount; i++) {
        // Create the random number
        int randomInteger = arc4random() % 10;
        // Build the Augend First ( Top Level )
        if(i <= equationNumberCountAugend) {
            // prevent the most significant digit from being 0.
            if([bucketAugend count] == 0){
                if(randomInteger == 0){
                    do{
                        randomInteger = arc4random() % 10;
                    }while(randomInteger == 0);
                }
            }
            [positionWithValueAugend setValue:[[self numberValues] objectAtIndex:randomInteger] forKey:[placeSystemValues objectAtIndex:(equationNumberCountAugend - i)]];
            
            CCLabelTTF* randomNumber = [CCLabelTTF labelWithString:[NSString stringWithFormat:@"%d", randomInteger] fontName:@"Marker Felt" fontSize:fontSize_];
            [bucketAugend addObject: randomNumber];
            
            randomNumberSprite = [CCSprite spriteWithFile:IMAGE_BLANK_TILE_67_X_67]; // rect:inputBackgroundDim_];
            //randomNumberSprite = [CCSprite spriteWithFile:@"button_background.png" rect:inputBackgroundDim_]; //
            float spriteHeight  = [randomNumberSprite boundingBox].size.height;
            float spriteWidth   = [randomNumberSprite boundingBox].size.width;
            [randomNumberSprite addChild:randomNumber];            
            [randomNumber setPosition:ccp(spriteWidth/2, spriteHeight/2)];

            randomNumberSprite.position = ccp(self.screenWidth / 2 + (spriteWidth * (i - 1)), self.screenHeight / 2);
        } else {
            // prevent the most significant digit from being 0.
            if([bucketAddend count] == 0){
                if(randomInteger == 0){
                    do{
                        randomInteger = arc4random() % 10;
                    }while(randomInteger == 0);
                }
            }
            
            // we need to subtract the Addend value from the index (i) - the Augend count
            // so: Addend - ( i - Augend ) = correct place system value
            [positionWithValueAddend setValue:[[self numberValues] objectAtIndex:randomInteger] forKey:[placeSystemValues objectAtIndex:(equationNumberCountAddend - (i - equationNumberCountAugend))]];
            
            CCLabelTTF* randomNumber = [CCLabelTTF labelWithString:[NSString stringWithFormat:@"%d", randomInteger] fontName:@"Marker Felt" fontSize:fontSize_];
            [bucketAddend addObject:randomNumber];
            
            randomNumberSprite = [CCSprite spriteWithFile:IMAGE_BLANK_TILE_67_X_67];
            //randomNumberSprite = [CCSprite spriteWithFile:@"button_background.png"];
            //[randomNumberSprite setTextureRect:inputBackgroundDim_]; // this can go in the line above
            
            float spriteHeight  = [randomNumberSprite boundingBox].size.height;
            float spriteWidth   = [randomNumberSprite boundingBox].size.width;

            [randomNumberSprite addChild:randomNumber];
            [randomNumber setPosition:ccp(spriteWidth/2, spriteHeight/2)];
            
            randomNumberSprite.position = ccp(self.screenWidth / 2 + (spriteWidth * ((i - 1) - equationNumberCountAddend)), self.screenHeight / 2 - spriteHeight);
        }
        
        [self addChild: randomNumberSprite];
    }

}

- (void)logAppState{
    NSLog(@" ");
    NSLog(@"###### Log app state ###### ");
    NSLog(@" ");
    NSLog(@"------------Augend position values----------------");

    
    // Show the Place System Values for the Augend
    for(id key in positionWithValueAugend) {
        id value = [positionWithValueAugend objectForKey:key];
        NSLog(@"positionWithValueAugend key: %@, Value: %@",key, value);
    }
    
    NSLog(@" ");
    NSLog(@"------------Addend position values-------------");

    
    // Show the Place System Values for the Addend
    for(id key in positionWithValueAddend) {
        id value = [positionWithValueAddend objectForKey:key];
        NSLog(@"positionWithValueAddend key: %@, Value: %@",key, value);
    }
    
    NSLog(@" ");
    NSLog(@"------------CARRY position values-----------------");

    
    // Show the Place System Values for the Carry
    for(id key in positionWithValueCarry) {
        id value = [positionWithValueCarry objectForKey:key];
        if([value isKindOfClass:[CCSprite class]]){
            CCLabelTTF *label = ((CCLabelTTF*) [value children].lastObject);
            NSLog(@"log positionWithValueCarry1 key: %@, Value: %@",key, [label string]);
        }else{
            NSLog(@"log positionWithValueCarry2 key: %@, Value: %@",key, value);
        }
    }
    
    NSLog(@" ");
    NSLog(@"------------SUM position values------------");

    
    // Show the Place System Values for the Sum
    for(id key in positionWithValueSum) {
        id value = [positionWithValueSum objectForKey:key];
        if([value isKindOfClass:[CCSprite class]]){
            CCLabelTTF *label = ((CCLabelTTF*) [value children].lastObject);
            NSLog(@"log positionWithValueSum1 key: %@, Value: %@",key, [label string]);
        }else{
            NSLog(@"log positionWithValueSum2 key: %@, Value: %@",key, value);
        }
    }
    
    NSLog(@" ");
    NSLog(@"------------Cloned numbers array------------");
    
    NSEnumerator *c = [clonedNumbers_ objectEnumerator];
    id clone;
    while (clone = [c nextObject]) {
        CCLabelTTF *piece = ((CCLabelTTF *) [clone children].lastObject);
        NSLog(@"Cloned piece:  %@", [piece string]);
    }
    
//    NSLog(@" ");
//    
//    NSLog(@"------------Number of sprites on this layer---------------");
//    
//    NSLog(@"self children :  %d",[[self children ] count]);
//    
//    NSLog(@"--------------------");

}

#pragma mark - Application State Toggle
-(void)_clearPlaceHoldersPreview{

    //  go through each place holder sprite and set to '  '.
    
    // carry position place holders.
    NSEnumerator *c = [carryPositions objectEnumerator];
    id carryObject;
    
    while (carryObject = [c nextObject]) {
        CCLabelTTF *targetPiece = ((CCLabelTTF *) [carryObject children].lastObject);
        [targetPiece setString:@"  "];    
    }
    
    // sum position place holders.       
    NSEnumerator *s = [sumPositions objectEnumerator];
    id sumObject;
    while (sumObject = [s nextObject]) {
        CCLabelTTF *targetPiece = ((CCLabelTTF *) [sumObject children].lastObject);
        [targetPiece setString:@"  "];
    }
    
}

#pragma mark - Application State Toggle
-(void)draw
 {
     glEnable(GL_LINE_SMOOTH);
     
     glColor4ub(255, 255, 255, 255);
     
     glLineWidth(lineWidth_);
     ccDrawLine(ccp((self.equationLine_x_start),self.equationLine_y), ccp(self.equationLine_x_end, self.equationLine_y));
     
     // restore default line width    
     glLineWidth(1);
     
     // restore disabled anti-aliasing
     glDisable(GL_LINE_SMOOTH);
 }
 
-(CGPoint)getNumberMenuItemCoordinates:(CCSprite*)clonedNumber{
    CCLabelTTF * child = ((CCLabelTTF *)[clonedNumber children].lastObject);
    
    
    NSString *numberLabel = [child string];
    CGFloat numberWidth     = [clonedNumber boundingBox].size.width;
    CGFloat numberHeight    = [clonedNumber boundingBox].size.height;
    
    float _xCoordinate = 0;
    float _yCoordinate = 0;
    
    switch (numberMenuLayoutOrientation_) {
        case LAYOUT_RHS:
            _xCoordinate = (self.screenWidth - numberWidth);
            break;
        case LAYOUT_LHS:
            _xCoordinate = (0 + (numberWidth/2) + PADDING_HORIZONTAL);
            break;
        default:
            break;
    }
    
    switch (numberMenuLayoutColumns_) {
        case LAYOUT_ONE_COL:
            _yCoordinate = (self.screenHeight - numberHeight);
            break;
        case LAYOUT_TWO_COL:
            _yCoordinate = (self.screenHeight - numberHeight);
            break;
        default:
            break;
    }
    
    if([numberLabel isEqualToString: @"0"]) {
       return ccp(_xCoordinate, _yCoordinate - (0 * numberHeight));
    }
    else if([numberLabel isEqualToString: @"1"]) {
        return ccp(_xCoordinate, _yCoordinate - (1 * numberHeight));
    }
    else if([numberLabel isEqualToString: @"2"]) {
        return ccp(_xCoordinate, _yCoordinate - (2 * numberHeight));
    }
    else if([numberLabel isEqualToString: @"3"]) {
        return ccp(_xCoordinate, _yCoordinate - (3 * numberHeight));
    }
    else if([numberLabel isEqualToString: @"4"]) {
        return ccp(_xCoordinate, _yCoordinate - (4 * numberHeight));
    }
    else if([numberLabel isEqualToString: @"5"]) {
        return ccp(_xCoordinate, _yCoordinate - (5 * numberHeight));
    }
    else if([numberLabel isEqualToString: @"6"]) {
        return ccp(_xCoordinate, _yCoordinate - (6 * numberHeight));
    }
    else if([numberLabel isEqualToString: @"7"]) {
        return ccp(_xCoordinate, _yCoordinate - (7 * numberHeight));
    }
    else if([numberLabel isEqualToString: @"8"]) {
        return ccp(_xCoordinate, _yCoordinate - (8 * numberHeight));
    }
    else if([numberLabel isEqualToString: @"9"]) {
        return ccp(_xCoordinate, _yCoordinate - (9 * numberHeight));
    } 
    
    // should never happen.
    return ccp(-1,-1);
    
}

-(void)setNumberPositionOnMenu:(CCSprite*)clonedNumber{
    clonedNumber.position = [self getNumberMenuItemCoordinates:clonedNumber];
}

-(CCSprite*)getNextHintPosition{
    NSLog(@"getNextHintPosition");
    NSLog(@"Size of array: %d", [sumPositions count]);
    
    int placeSystemIndexCount   = [placeSystemValues count];
    int currentMultiplier;
    
    // this value will choose the larger of the two - addend length and augend length.  then add 1.  the 1 is b/c two numbers of length x can only be as long as x + 1.
    placeSystemIndexCount = (CONFIG_SESSION_EQUATION_NUMBER_COUNT_ADDEND > CONFIG_SESSION_EQUATION_NUMBER_COUNT_AUGEND) ? CONFIG_SESSION_EQUATION_NUMBER_COUNT_ADDEND : CONFIG_SESSION_EQUATION_NUMBER_COUNT_AUGEND;
    
    placeSystemIndexCount++;
    
    for (int i = 0; i < placeSystemIndexCount; i++)
    {
        NSLog(@"------------get hint position loop--------------------");
        NSLog(@"THIS IS I: %d", i);

        currentMultiplier = [(NSNumber *)[placeSystemValues objectAtIndex:i] intValue];
        NSLog(@"currentMultiplier: %d", currentMultiplier);
        
        int numberColumnCorrectAnswer;
        numberColumnCorrectAnswer = [self isColumnAnswerCorrect:i];
        
        id numberValueCarry     = [positionWithValueCarry objectForKey:[NSNumber numberWithInt:currentMultiplier]];

        // This should be nil or zero
        id numberValueAugendPrevious = [positionWithValueAugend objectForKey:[NSNumber numberWithInt:currentMultiplier]];
        id numberValueAddendPrevious = [positionWithValueAddend objectForKey:[NSNumber numberWithInt:currentMultiplier]];

        if(i > 0) {
            numberValueAugendPrevious = [positionWithValueAugend objectForKey:[NSNumber numberWithInt:(currentMultiplier / 10)]];
            numberValueAddendPrevious = [positionWithValueAddend objectForKey:[NSNumber numberWithInt:(currentMultiplier / 10)]];
        }

        id numberValueSum= [positionWithValueSum objectForKey:[NSNumber numberWithInt:currentMultiplier]];
        
        NSLog(@"numberValueCarry: %@", numberValueCarry);
        NSLog(@"numberValueAugendPrevious: %@", numberValueAugendPrevious);
        NSLog(@"numberValueAddendPrevious: %@", numberValueAddendPrevious);
        NSLog(@"numberValueSum: %@", numberValueSum);
        
        if (numberValueCarry == nil && numberValueAugendPrevious == nil && numberValueAddendPrevious == nil && numberValueSum == nil)
        {
            return nil;
        }
        
        int numberCarry;
        int numberAugendPrevious;
        int numberAddendPrevious;
        
        // AUGEND
        if([numberValueAugendPrevious isKindOfClass:[CCSprite class]]){
            CCLabelTTF * numberValueAugendPreviousTTF = ((CCLabelTTF*)[numberValueAugendPrevious children].lastObject);
                numberAugendPrevious = [[numberValueAugendPreviousTTF string] intValue];
        } else {
            numberAugendPrevious = [numberValueAugendPrevious intValue];
        }
        NSLog(@"numberAugendPrevious: %d", numberAugendPrevious);

        // END AUGEND
        
        // ADDEND
        if([numberValueAddendPrevious isKindOfClass:[CCSprite class]]){
            CCLabelTTF * numberValueAddendPreviousTTF = ((CCLabelTTF*)[numberValueAddendPrevious children].lastObject);
            numberAddendPrevious = [[numberValueAddendPreviousTTF string] intValue];
            
        } else {
            numberAddendPrevious = [numberValueAddendPrevious intValue];
        }
        NSLog(@"numberAddendPrevious: %d", numberAddendPrevious);

        // END ADDEND

        // CARRY
        if([numberValueCarry isKindOfClass:[CCSprite class]]){
            CCLabelTTF * numberValueCarryTTF = ((CCLabelTTF*)[numberValueCarry children].lastObject);
            numberCarry = [[numberValueCarryTTF string] intValue];
            
        } else {
            numberCarry = [numberValueCarry intValue];
        }
        NSLog(@"numberCarry: %d", numberCarry);

        // END CARRY
        
        if (((numberAugendPrevious + 1) > 9) && (i == (placeSystemIndexCount - 1))) {
            numberCarry = 1;
        }
        
        if((numberAddendPrevious + numberAugendPrevious + numberCarry) > 9 && i != 0)
        {
            if([numberValueCarry isKindOfClass:[CCSprite class]] && ![numberValueSum isKindOfClass:[CCSprite class]])
            {
                if (numberColumnCorrectAnswer != -99)
                {
                    return [sumPositions objectAtIndex:(([sumPositions count] -1 ) - i)];
                }
                else
                {
                    return [carryPositions objectAtIndex:(([carryPositions count]) - i)];
                }
            }
            
            if([numberValueSum isKindOfClass:[CCSprite class]] && ![numberValueCarry isKindOfClass:[CCSprite class]])
            {
                if (numberColumnCorrectAnswer != -99)
                {
                    return [carryPositions objectAtIndex:(([carryPositions count]) - i)];
                }
                else
                {
                    return [sumPositions objectAtIndex:(([sumPositions count] -1 ) - i)];
                }
                continue;
            }
            else
            {
                NSLog(@"size of sum positions array:  %d", [sumPositions count]);
                
                NSLog(@"returning object at index in sums:  %d", ([sumPositions count] -1) - i);
                
                // check carry is correct
                if (numberColumnCorrectAnswer != -99)
                {
                    //return [numbersMenuItems_ objectAtIndex:numberColumnCorrectAnswer];
                    return [carryPositions objectAtIndex:(([carryPositions count]) - i)];
                }
                
                return [sumPositions objectAtIndex:(([sumPositions count] -1 ) - i)];
            }
            
            
//            /////////////
//            if([numberValueCarry isKindOfClass:[CCSprite class]])
//            {
//                //continue;
//                NSLog(@"continuing getnext sum position object at index:  %d", i);
//                
//                // check carry is correct
//                if (numberColumnCorrectAnswer != -99)
//                {
//                    //return [numbersMenuItems_ objectAtIndex:numberColumnCorrectAnswer];
//                    return [carryPositions objectAtIndex:(([carryPositions count]) - i)];
//                }
//                
//                if([numberValueSum isKindOfClass:[CCSprite class]])
//                {
//                    if (numberColumnCorrectAnswer != -99)
//                    {
//                        //return [numbersMenuItems_ objectAtIndex:numberColumnCorrectAnswer];
//                        return [sumPositions objectAtIndex:(([sumPositions count] -1 ) - i)];
//                    }
//                    continue;
//                }
//                else
//                {
//                    NSLog(@"size of sum positions array:  %d", [sumPositions count]);
//                    
//                    NSLog(@"returning object at index in sums:  %d", ([sumPositions count] -1) - i);
//                    
//                    // check carry is correct
//                    if (numberColumnCorrectAnswer != -99)
//                    {
//                        //return [numbersMenuItems_ objectAtIndex:numberColumnCorrectAnswer];
//                        return [carryPositions objectAtIndex:(([carryPositions count]) - i)];
//                    }
//                    
//                    return [sumPositions objectAtIndex:(([sumPositions count] -1 ) - i)];
//                }                
//            }
//            else
//            {
//                NSLog(@"size of carry positions array:  %d", [carryPositions count]);
//                NSLog(@"returning object at index in carry:  %d", ([carryPositions count]) - i);
//                return [carryPositions objectAtIndex:(([carryPositions count]) - i)];
//            }
        }
        else
        {
            NSLog(@"continuing getnext sum position object at index:  %d", i);
            if([numberValueSum isKindOfClass:[CCSprite class]])
            {
                if (numberColumnCorrectAnswer != -99)
                {
                    //return [numbersMenuItems_ objectAtIndex:numberColumnCorrectAnswer];
                    return [sumPositions objectAtIndex:(([sumPositions count] -1 ) - i)];
                }

                continue;
            }
            else
            {
                NSLog(@"size of sum positions array:  %d", [sumPositions count]);
                NSLog(@"returning object at index in sums:  %d", ([sumPositions count] -1) - i);
                return [sumPositions objectAtIndex:(([sumPositions count] -1 ) - i)];
            }
        }
    }
    NSLog(@"END*****************************************");
}

-(CCSprite*)getNextHintValue{
    int enteredValueCarry       = 0;
    int enteredValueCarryNext   = 0;
    int augendValue             = 0;
    int addendValue             = 0;
    int previousAugendValue     = 0;
    int previousAddendValue     = 0;
    int enteredValueSum         = 0;
    int placeSystemIndexCount   = [placeSystemValues count];
    int currentMultiplier;
    int previousMultiplier;
    
    // this value will choose the larger of the two - addend length and augend length.  then add 1.  the 1 is b/c two numbers of length x can only be as long as x + 1.
    placeSystemIndexCount = (CONFIG_SESSION_EQUATION_NUMBER_COUNT_ADDEND > CONFIG_SESSION_EQUATION_NUMBER_COUNT_AUGEND) ? CONFIG_SESSION_EQUATION_NUMBER_COUNT_ADDEND : CONFIG_SESSION_EQUATION_NUMBER_COUNT_AUGEND;
    
    placeSystemIndexCount++;
    
    
    for (int i = 0; i < placeSystemIndexCount; i++)
    {
        int numberColumnCorrectAnswer;
        
        numberColumnCorrectAnswer = [self isColumnAnswerCorrect:i];
        
        // COLUMN_IS_CORRECT is given an issue
        if (numberColumnCorrectAnswer != -99)
        {
            return [numbersMenuItems_ objectAtIndex:numberColumnCorrectAnswer];
        }
        
        currentMultiplier = [(NSNumber *)[placeSystemValues objectAtIndex:i] intValue];
        if(i > 0) {
            previousMultiplier = [(NSNumber *)[placeSystemValues objectAtIndex:(i - 1)] intValue];
        }
        
        NSLog(@"---------------getNextHintValue Loop-----------------------");
        NSLog(@"Index At: %d", i);
        NSLog(@"currentMultiplier: %d", currentMultiplier);

        id numberValueCarry     = [positionWithValueCarry objectForKey:[NSNumber numberWithInt:currentMultiplier]];
        id numberValueCarryNext = [positionWithValueCarry objectForKey:[NSNumber numberWithInt:(currentMultiplier * 10)]];
        id numberValueAugend    = [positionWithValueAugend objectForKey:[NSNumber numberWithInt:currentMultiplier]];
        id numberValueAddend    = [positionWithValueAddend objectForKey:[NSNumber numberWithInt:currentMultiplier]];
        id numberValueSum       = [positionWithValueSum objectForKey:[NSNumber numberWithInt:currentMultiplier]];
        
        id numberValuePreviousAugend = [positionWithValueAugend objectForKey:[NSNumber numberWithInt:previousMultiplier]];
        id numberValuePreviousAddend = [positionWithValueAddend objectForKey:[NSNumber numberWithInt:previousMultiplier]];

        NSLog(@"numberValueCarry: %@", numberValueCarry);
        NSLog(@"numberValueCarryNext: %@", numberValueCarryNext);
        NSLog(@"numberValueAugend: %@", numberValueAugend);
        NSLog(@"numberValueAddend: %@", numberValueAddend);
        NSLog(@"numberValueSum: %@", numberValueSum);
        NSLog(@"numberValuePreviousAugend: %@", numberValuePreviousAugend);
        NSLog(@"numberValuePreviousAddend: %@", numberValuePreviousAddend);
        
        int numberCarry;
        int numberCarryNext;
        int numberPreviousCarry = 0;
        int numberAugend;
        int numberAddend;
        int numberSum;
        int numberCurrentColumn;
        int numberPreviousAugend;
        int numberPreviousAddend;
        
        // Refactor these
        // CARRY
        if([numberValueCarry isKindOfClass:[CCSprite class]]){
            CCLabelTTF * numberValueCarryTTF = ((CCLabelTTF*)[numberValueCarry children].lastObject);
            numberCarry = [[numberValueCarryTTF string] intValue];
            
            if( numberCarry > 1 ) {
                return [numbersMenuItems_ objectAtIndex:1];
            }
            
        } else {
            numberCarry = 0;
        }
        
        NSLog(@"numberCarry: %d", numberCarry);
        if(numberCarry >= 0)
        {
            enteredValueCarry = numberCarry;
            NSLog(@"enteredValueCarry: %d", enteredValueCarry);
        }
        // END CARRY
        
        // AUGEND
        if([numberValueAugend isKindOfClass:[CCSprite class]]){
            CCLabelTTF * numberValueAugendTTF = ((CCLabelTTF*)[numberValueAugend children].lastObject);            
            numberAugend = [[numberValueAugendTTF string] intValue];
            
        } else {
            numberAugend = [numberValueAugend intValue];
        }
        
        NSLog(@"numberAugend: %d", numberAugend);
        if(numberAugend >= 0)
        {
            augendValue = numberAugend;
        }
        // END AUGEND
        
        // ADDEND
        if([numberValueAddend isKindOfClass:[CCSprite class]]){
            CCLabelTTF * numberValueAddendTTF = ((CCLabelTTF*)[numberValueAddend children].lastObject);
            numberAddend = [[numberValueAddendTTF string] intValue];
        } else {
            numberAddend = [numberValueAddend intValue];
        }
        
        NSLog(@"numberAddend: %d", numberAddend);
        if(numberAddend >= 0)
        {
            addendValue = numberAddend;
        }
        // END ADDEND
        
        // PREVIOUS AUGEND
        if([numberValuePreviousAugend isKindOfClass:[CCSprite class]]){
            CCLabelTTF * numberValuePreviousAugendTTF = ((CCLabelTTF*)[numberValuePreviousAugend children].lastObject);
            numberPreviousAugend = [[numberValuePreviousAugendTTF string] intValue];
            
        } else {
            numberPreviousAugend = [numberValuePreviousAugend intValue];
        }
        
        NSLog(@"numberPreviousAugend: %d", numberPreviousAugend);
        if(numberPreviousAugend >= 0)
        {
            previousAugendValue = numberPreviousAugend;
        }
        // END PREVIOUS AUGEND
        
        // PREVIOUS ADDEND
        if([numberValuePreviousAddend isKindOfClass:[CCSprite class]]){
            CCLabelTTF * numberValuePreviousAddendTTF = ((CCLabelTTF*)[numberValuePreviousAddend children].lastObject);
            numberPreviousAddend = [[numberValuePreviousAddendTTF string] intValue];
            
        } else {
            numberPreviousAddend = [numberValuePreviousAddend intValue];
        }
        
        NSLog(@"numberPreviousAddend: %d", numberPreviousAddend);
        if(numberPreviousAddend >= 0)
        {
            previousAddendValue = numberPreviousAddend;
        }
        // END PREVIOUS ADDEND

        // COLUMN ONES
        numberCurrentColumn = (numberAugend + numberAddend);
        NSLog(@"numberCurrentColumn: %d", numberCurrentColumn);
        
        // CARRY NEXT
        if([numberValueCarryNext isKindOfClass:[CCSprite class]]){
            CCLabelTTF * numberValueNextCarryTTF = ((CCLabelTTF*)[numberValueCarryNext children].lastObject);
            numberCarryNext = [[numberValueNextCarryTTF string] intValue];
        } else {
            numberCarryNext = 0;
        }
        
        NSLog(@"numberCarryNext: %d", numberCarryNext);
        // Do I really need this?
        if(numberCarryNext >= 0)
        {
            enteredValueCarryNext = numberCarryNext;        
        }
        // END CARRY NEXT

        
        // END COLUMN ONES
        NSLog(@"Entered Carry Value: %d", enteredValueCarry);
        NSLog(@"Entered Carry Value: %d", enteredValueCarry);
        NSLog(@"Entered Carry Next Value: %d", enteredValueCarryNext);
        NSLog(@"Entered Augend Value: %d", augendValue);
        NSLog(@"Entered Addend Value: %d", addendValue);
        NSLog(@"Entered Sum Value: %d", enteredValueSum);     
        
        NSLog(@"Sum Value should be (aug + add + entered carry): %d", (augendValue + addendValue + enteredValueCarry));
        NSLog(@"Entered Value is: %d", (enteredValueSum));
        NSLog(@"placeSystemIndexCount At: %d", placeSystemIndexCount);        
        numberPreviousCarry = 0;
        if(((previousAugendValue + previousAddendValue) > 9) && (i > 0)) {
            numberPreviousCarry = 1;
        } else if (((previousAugendValue + 1) > 9) && (i == (placeSystemIndexCount - 1))) {
            numberPreviousCarry = 1;
        }
        
        if (enteredValueCarry == 0 && enteredValueCarryNext == 0 && augendValue == 0 && addendValue == 0 && numberPreviousCarry == 0)
        {
            NSLog(@"******* ZERO HINT BLOCK **********");
             NSLog(@"numberValueCarry: %@", numberValueCarry);
             NSLog(@"numberValueSum: %@", numberValueSum);
            
            if (i > 1)
            {
                NSLog(@"previousAugendValue: %d", previousAugendValue);
                NSLog(@"previousAddendValue: %d", previousAddendValue);
                NSLog(@"numberPreviousCarry: %d", numberPreviousCarry);
            }
            NSLog(@"******* END ZERO HINT BLOCK **********");
            return nil;
        }
        
        if(((augendValue + addendValue + numberPreviousCarry) != (enteredValueSum))) {
            NSLog(@"numberPreviousCarry:  %d", numberPreviousCarry);
            NSLog(@"returning object at index:  %d", numberCurrentColumn);
            if(enteredValueCarry == 0 && numberPreviousCarry == 1) {
                NSLog(@"enteredValueCarry is zero:  %d", enteredValueCarry);
                return [numbersMenuItems_ objectAtIndex:1];
            }
            
            if(((augendValue + addendValue + enteredValueCarry) > 9) || ((augendValue + addendValue) > 9)) {
                
                if([numberValueSum isKindOfClass:[CCSprite class]]){
                    CCLabelTTF * numberValueSumTTF = ((CCLabelTTF*)[numberValueSum children].lastObject);
                    numberSum = [[numberValueSumTTF string] intValue];
                    continue;
                }
                
                // @TODO: Do we need to push the 1 value to the next carry position? 
                NSLog(@" > 9 returning object at index:  %d", ((augendValue + addendValue + enteredValueCarry) - 10));
                return [numbersMenuItems_ objectAtIndex:(((augendValue + addendValue + enteredValueCarry) - 10))];
            } else {
                // check previous Augend and Addend if > 9 else reset to zero
                if((numberPreviousCarry == 1) && (i > 0)) {
                    if(enteredValueCarry == 0) {
                        NSLog(@"enteredValueCarry is zero:  %d", enteredValueCarry);
                        return [numbersMenuItems_ objectAtIndex:1];
                    }
                }

                if([numberValueSum isKindOfClass:[CCSprite class]]){
                    CCLabelTTF * numberValueSumTTF = ((CCLabelTTF*)[numberValueSum children].lastObject);
                    numberSum = [[numberValueSumTTF string] intValue];
                    continue;
                }
                
                NSLog(@" < 9 returning object at index:  %d", ((augendValue + addendValue + enteredValueCarry)));
                return [numbersMenuItems_ objectAtIndex:(augendValue + addendValue + enteredValueCarry)];
            }
        }
    }
//    return [numbersMenuItems_ objectAtIndex:0];
    return nil;
}

-(void) resetInputScales{
    NSEnumerator *e = [sumPositions objectEnumerator];
    id current;
    
    while ( current= [e nextObject]) {
        CCSprite * currentSprite = ((CCSprite*)current);
        currentSprite.scaleX = 1;
        currentSprite.scaleY = 1;
    }
    
    NSEnumerator *f = [carryPositions objectEnumerator];
    
    while ( current= [f nextObject]) {
        CCSprite * currentSprite = ((CCSprite*)current);
        currentSprite.scaleX = 1;
        currentSprite.scaleY = 1;
    }
    
    NSEnumerator *g = [numbersMenuItems_ objectEnumerator];
    
    while ( current= [g nextObject]) {
        CCSprite * currentSprite = ((CCSprite*)current);
        currentSprite.scaleX = 1;
        currentSprite.scaleY = 1;
    }
}

-(void) resetInputColors{
    
    NSEnumerator *e = [sumPositions objectEnumerator];
    id current;
    while ( current= [e nextObject]) {
        CCSprite * currentSprite = ((CCSprite*)current);
        CCLabelTTF * currentTTF = (CCLabelTTF*)[[currentSprite children] lastObject];
        [currentTTF setColor:ccWHITE];
        [currentSprite setColor:ccBLUE];
    }
    
    NSEnumerator *f = [carryPositions objectEnumerator];
    
    while ( current= [f nextObject]) {
        CCSprite * currentSprite = ((CCSprite*)current);
        CCLabelTTF * currentTTF = (CCLabelTTF*)[[currentSprite children] lastObject];
        [currentTTF setColor:ccWHITE];
        [currentSprite setColor:ccBLUE];
    }
    
    NSEnumerator *g = [numbersMenuItems_ objectEnumerator];
    
    while ( current= [g nextObject]) {
        CCSprite * currentSprite = ((CCSprite*)current);
        CCLabelTTF * currentTTF = (CCLabelTTF*)[[currentSprite children] lastObject];
        [currentTTF setColor:ccWHITE];
    }
}

-(NSMutableArray*)getHint{
    // @TODO: Get Position and Value ( SPRITE ) for array
    ccColor3B inputBackgroundColorMagenta = ccc3(232,71,157);

    NSLog(@"$$$$$$$ getHint $$$$$$$$$$$");
    NSMutableArray * hintSpriteArray = [[NSMutableArray alloc] init];
    NSLog(@"before getNextHintPosition");
    
    CCSprite * hintPositionSprite = [self getNextHintPosition];
    CCSprite * nextHintValueSprite = [self getNextHintValue];

    if (nextHintValueSprite != nil)
    {
        NSLog(@"result of hint value:  %@", nextHintValueSprite);
        [hintSpriteArray addObject:nextHintValueSprite];
    }
    
    if(hintPositionSprite != nil && nextHintValueSprite != nil){
        NSLog(@"result of hint position:  %@", hintPositionSprite);
        [hintPositionSprite setColor:inputBackgroundColorMagenta];
        [hintSpriteArray addObject:hintPositionSprite];
    }
    
    return hintSpriteArray;
}

-(int)isColumnAnswerCorrect:(int)currentIndex{
    NSLog(@"isColumnAnswerCorrect");
    NSLog(@"currentIndex: %d", currentIndex);

    int currentMultiplier;
    int numberSum = 0;
    int numberCarry = 0;
    int numberAugend = 0;
    int numberAddend = 0;
    
    int numberAugendPrevious = 0;
    int numberAddendPrevious = 0;
    
    int actualSum = 0;
    
    currentMultiplier = [(NSNumber *)[placeSystemValues objectAtIndex:currentIndex] intValue];
    
    id numberValueCarry     = [positionWithValueCarry objectForKey:[NSNumber numberWithInt:currentMultiplier]];
    id numberValueAugend    = [positionWithValueAugend objectForKey:[NSNumber numberWithInt:currentMultiplier]];
    id numberValueAddend    = [positionWithValueAddend objectForKey:[NSNumber numberWithInt:currentMultiplier]];
    id numberValueSum       = [positionWithValueSum objectForKey:[NSNumber numberWithInt:currentMultiplier]];
    
    if (currentIndex >= 1)
    {
        id numberValueAugendPrevious    = [positionWithValueAugend objectForKey:[NSNumber numberWithInt:(currentMultiplier/10)]];
        id numberValueAddendPrevious    = [positionWithValueAddend objectForKey:[NSNumber numberWithInt:(currentMultiplier/10)]];
        
        // PREVIOUS AUGEND
        if([numberValueAugendPrevious isKindOfClass:[CCSprite class]]){
            CCLabelTTF * numberValueAugendPreviousTTF = ((CCLabelTTF*)[numberValueAugendPrevious children].lastObject);
            numberAugendPrevious = [[numberValueAugendPreviousTTF string] intValue];
        } else {
            numberAugendPrevious = [numberValueAugendPrevious intValue];
        }
        
        // PREVIOUS ADDEND
        if([numberValueAddendPrevious isKindOfClass:[CCSprite class]]){
            CCLabelTTF * numberValueAddendPreviousTTF = ((CCLabelTTF*)[numberValueAddendPrevious children].lastObject);
            numberAddendPrevious = [[numberValueAddendPreviousTTF string] intValue];
        } else {
            numberAddendPrevious = [numberValueAddendPrevious intValue];
        }
    }
    
    if([numberValueSum isKindOfClass:[CCSprite class]]){
        CCLabelTTF * numberValueSumTTF = ((CCLabelTTF*)[numberValueSum children].lastObject);
        numberSum = [[numberValueSumTTF string] intValue];
    }
    
    if([numberValueCarry isKindOfClass:[CCSprite class]]){
        CCLabelTTF * numberValueCarryTTF = ((CCLabelTTF*)[numberValueCarry children].lastObject);
        numberCarry = [[numberValueCarryTTF string] intValue];
    }
    
    // AUGEND
    if([numberValueAugend isKindOfClass:[CCSprite class]]){
        CCLabelTTF * numberValueAugendTTF = ((CCLabelTTF*)[numberValueAugend children].lastObject);
        numberAugend = [[numberValueAugendTTF string] intValue];
    } else {
        numberAugend = [numberValueAugend intValue];
    }
    
    // ADDEND
    if([numberValueAddend isKindOfClass:[CCSprite class]]){
        CCLabelTTF * numberValueAddendTTF = ((CCLabelTTF*)[numberValueAddend children].lastObject);
        numberAddend = [[numberValueAddendTTF string] intValue];
    } else {
        numberAddend = [numberValueAddend intValue];
    }
    
    actualSum = numberAugend + numberAddend;
    
    if(numberAugendPrevious + numberAddendPrevious > 9)
    {
        actualSum++;
    }
    
    if (actualSum > 9)
    {
        actualSum -= 10;
    }

    if ((numberAugendPrevious + numberAddendPrevious > 9) && numberCarry != 1)
    {
        return 1;
    }
    
    if (numberSum != actualSum )
    {
        return actualSum;
    }
    
    return COLUMN_IS_CORRECT;
}

-(BOOL)isAnswerCorrect{

    int enteredValueCarry       = 0;
    int enteredValueCarryNext   = 0;
    int augendValue             = 0;
    int addendValue             = 0;
    int enteredValueSum         = 0;
    int currentMultiplier;
    
    // Demo Mode

    NSLog(@"Are we here?");
    for (int i = 0; i < [placeSystemValues count]; i++)
    {
        currentMultiplier = [(NSNumber *)[placeSystemValues objectAtIndex:i] intValue];
        NSLog(@"currentMultiplier: %d", currentMultiplier);
        
        id numberValueCarry     = [positionWithValueCarry objectForKey:[NSNumber numberWithInt:currentMultiplier]];
        id numberValueCarryNext = [positionWithValueCarry objectForKey:[NSNumber numberWithInt:(currentMultiplier * 10)]];
        id numberValueAugend    = [positionWithValueAugend objectForKey:[NSNumber numberWithInt:currentMultiplier]];
        id numberValueAddend    = [positionWithValueAddend objectForKey:[NSNumber numberWithInt:currentMultiplier]];
        id numberValueSum       = [positionWithValueSum objectForKey:[NSNumber numberWithInt:currentMultiplier]];
        
        NSLog(@"numberValueCarry: %@", numberValueCarry);
        NSLog(@"numberValueCarryNext: %@", numberValueCarryNext);
        NSLog(@"numberValueAugend: %@", numberValueAugend);
        NSLog(@"numberValueAddend: %@", numberValueAddend);
        NSLog(@"numberValueSum: %@", numberValueSum);
        
        int numberCarry;
        int numberCarryNext;
        int numberAugend;
        int numberAddend;
        int numberSum;
        int numberCurrentColumn;
                
        // Refactor these
        // CARRY
        if([numberValueCarry isKindOfClass:[CCSprite class]]){
            CCLabelTTF * numberValueCarryTTF = ((CCLabelTTF*)[numberValueCarry children].lastObject);

            NSLog(@"positionWithValueCarry key: %d, Value: %@",currentMultiplier, numberValueCarryTTF);
            NSLog(@"num: %@", numberValueCarryTTF);
            
            numberCarry = [[numberValueCarryTTF string] intValue];
        } else {
            numberCarry = 0;
        }
        
        NSLog(@"numberCarry: %d", numberCarry);
        if(numberCarry >= 0)
        {
            enteredValueCarry = enteredValueCarry + (numberCarry * currentMultiplier);
            NSLog(@"enteredValueCarry: %d", enteredValueCarry);
        }
        // END CARRY
        
        // CARRY NEXT
        if([numberValueCarryNext isKindOfClass:[CCSprite class]]){
            CCLabelTTF * numberValueNextCarryTTF = ((CCLabelTTF*)[numberValueCarryNext children].lastObject);
            NSLog(@"positionWithValueCarryNext key: %d, Value: %@",currentMultiplier, numberValueNextCarryTTF);
            NSLog(@"num: %@", numberValueNextCarryTTF);
        
            numberCarryNext = [[numberValueNextCarryTTF string] intValue];
        } else {
            numberCarryNext = 0;
        }
        
        NSLog(@"numberCarryNext: %d", numberCarryNext);
        // Do I really need this?
        if(numberCarryNext >= 0)
        {
            enteredValueCarryNext = enteredValueCarryNext + (numberCarryNext * currentMultiplier);
        }
        // END CARRY NEXT
        
        // AUGEND
        if([numberValueAugend isKindOfClass:[CCSprite class]]){
            CCLabelTTF * numberValueAugendTTF = ((CCLabelTTF*)[numberValueAugend children].lastObject);
            NSLog(@"positionWithValueAugend key: %d, Value: %@",currentMultiplier, numberValueAugendTTF);
            NSLog(@"num: %@", numberValueAugendTTF);
        
            numberAugend = [[numberValueAugendTTF string] intValue];
            
        } else {
            NSLog(@"Augend Value: %@", numberValueAugend);
            numberAugend = [numberValueAugend intValue];
        }
        
        NSLog(@"numberAugend: %d", numberAugend);
        if(numberAugend >= 0)
        {
            augendValue = augendValue + (numberAugend * currentMultiplier);
        }
        // END AUGEND
        
        // ADDEND
        if([numberValueAddend isKindOfClass:[CCSprite class]]){
            CCLabelTTF * numberValueAddendTTF = ((CCLabelTTF*)[numberValueAddend children].lastObject);
            NSLog(@"positionWithValueAddend key: %d, Value: %@",currentMultiplier, numberValueAddendTTF);
            NSLog(@"num: %@", numberValueAddendTTF);
        
            numberAddend = [[numberValueAddendTTF string] intValue];
            
        } else {
            NSLog(@"Addend Value: %@", numberValueAddend);
            numberAddend = [numberValueAddend intValue];
        }
        
        NSLog(@"numberAddend: %d", numberAddend);
        if(numberAddend >= 0)
        {
            addendValue = addendValue + (numberAddend * currentMultiplier);
        }
        // END ADDEND
        
        // SUM
        if([numberValueSum isKindOfClass:[CCSprite class]]){
            CCLabelTTF * numberValueSumTTF = ((CCLabelTTF*)[numberValueSum children].lastObject);
            NSLog(@"positionWithValueSum key: %d, Value: %@",currentMultiplier, numberValueSumTTF);
            NSLog(@"num: %@", numberValueSumTTF);
        
            numberSum = [[numberValueSumTTF string] intValue];
           
        } else {
            numberSum = 0;
        }
        
         NSLog(@"numberSum: %d", numberSum);
        if(numberSum >= 0)
        {
            enteredValueSum = enteredValueSum + (numberSum * currentMultiplier);
        }
        // END SUM
        
        // COLUMN ONES
        numberCurrentColumn = (numberAugend + numberAddend);
        NSLog(@"=============================================\n=============================================");
        NSLog(@"numberCurrentColumn: %d", numberCurrentColumn);
        NSLog(@"=============================================\n=============================================");

        
        if(numberCurrentColumn > 9) {
            // we need to get the next column value
            
            NSLog(@"=============================================");
            NSLog(@"numberCarry: %d", numberCarry);
            NSLog(@"numberCarryNext: %d", numberCarryNext);
            NSLog(@"=============================================");
            
            // We need to validate the number in the carry
            //if(numberCarryNext >= 0 && numberCarryNext != 1) {
            if(numberCarryNext != 1) {
                NSLog(@"=============================================");
                NSLog(@"BREAK!");
                NSLog(@"BREAK!");
                NSLog(@"BREAK!");
                NSLog(@"=============================================");
                
                return NO;
            }
        }
        
        // END COLUMN ONES
        NSLog(@"=======================In Loop======================");
        NSLog(@"Entered Carry Value: %d", enteredValueCarry);
        NSLog(@"Entered Carry Next Value: %d", enteredValueCarryNext);
        NSLog(@"Entered Augend Value: %d", augendValue);
        NSLog(@"Entered Addend Value: %d", addendValue);
        NSLog(@"Entered Sum Value: %d", enteredValueSum);     
        NSLog(@"=============================================");
        
    }
    NSLog(@"Entered Carry Value: %d", enteredValueCarry);
    NSLog(@"Entered Carry Next Value: %d", enteredValueCarryNext);
    NSLog(@"Entered Augend Value: %d", augendValue);
    NSLog(@"Entered Addend Value: %d", addendValue);
    NSLog(@"Entered Sum Value: %d", enteredValueSum);
    
    // Check the Carry
    //int n; // from somewhere
    //while (n) { digit = n % 10; n /= 10; }
    
    NSLog(@"Sum Value shoud be: %d", (augendValue + addendValue));
    
    if(((augendValue + addendValue) == enteredValueSum)) {
        if(augendValue == 0 && addendValue == 0 && enteredValueSum == 0)
            return NO;
        
        return YES;
    }
    
    // http://stackoverflow.com/questions/11571629/concatenate-all-values-in-nsmutabledictionary-cast-to-int-nsinteger-and-nsstrin
    
    return NO;

}

#pragma mark - Touch Methods -
-(BOOL) ccTouchBegan:(UITouch*)touch withEvent:(UIEvent *)event
{
	CGPoint touchLocation=[self locationFromTouch:touch];    
    // Check to see if number should be cloned.
    NSEnumerator *e = [numbersMenuItems_ objectEnumerator];
    id numberMenuItem;
    
    while (numberMenuItem = [e nextObject]) {        
        CCSprite *item = ((CCSprite *)numberMenuItem);
        if([self isTouchForMe:touchLocation :item]){
            item.scaleX = VIZ_FX_SCALE_X;
            item.scaleY = VIZ_FX_SCALE_Y;
            currentlySelectedNumber_ = item;
            
            CCLabelTTF * childItem = ((CCLabelTTF *)[item children].lastObject);
            
            NSString *numberLabel = [childItem string];
            NSLog(@"numberLabel %@", numberLabel);
            
            // @TODO: This should pull the audio file from the index: numberLabel
            
            CCLabelTTF *clonedNumber = [CCLabelTTF labelWithString:numberLabel fontName:@"Marker Felt" fontSize:fontSize_];           
            
            CCSprite * clonedSprite = [CCSprite spriteWithFile:IMAGE_BLANK_TILE_67_X_67]; // rect:inputBackgroundDim_];
            //CCSprite * clonedSprite = [CCSprite spriteWithFile:@"button_background.png" rect:inputBackgroundDim_];
            
            float spriteHeight  = [clonedSprite boundingBox].size.height;
            float spriteWidth   = [clonedSprite boundingBox].size.width;
            
            [clonedNumber setPosition:ccp(spriteWidth/2, spriteHeight/2)];
            
            [clonedSprite addChild:clonedNumber z: 1];
            
            //[clonedNumber setOpacity:0]; //transparent
            //[clonedNumber setOpacity:255]; //solid
            [clonedSprite setOpacity:128]; //semi-transparent
            
            [self addChild:clonedSprite];
            [clonedNumbers_ addObject:clonedSprite];
            
            [self setNumberPositionOnMenu:clonedSprite];
        }
    
    }
    
    NSEnumerator *c = [clonedNumbers_ objectEnumerator];
    id clonedNumber;
    while (clonedNumber = [c nextObject]) {
        NSLog(@"Starting touch begin8");
        CCSprite *item = ((CCSprite *)clonedNumber);
        if([self isTouchForMe:touchLocation :item]){
            [[SimpleAudioEngine sharedEngine] playEffect:SOUND_FX_PRESS];
            activeSprite_ = item;
            item.position = touchLocation;
            item.scaleX = VIZ_FX_SCALE_X;
            item.scaleY = VIZ_FX_SCALE_Y;
            return YES;
        }
    }
    // nothing
    activeSprite_ = nil;
    return NO;
}

-(void) _reset{
    NSEnumerator *e = [clonedNumbers_ objectEnumerator];
    id numberMenuItem;
    
    NSLog(@"BEFORE Cloned Count: %d", [clonedNumbers_ count]);
    
    if(currentlySelectedNumber_ != nil){
        currentlySelectedNumber_.scaleX = 1;
        currentlySelectedNumber_.scaleY = 1;
    }
    while (numberMenuItem = [e nextObject]) {
        CCSprite *number = ((CCSprite *)numberMenuItem);
        [number stopAllActions];
        
        number.scaleX = VIZ_FX_SCALE_X;
        number.scaleY = VIZ_FX_SCALE_Y;
        [self removeChild:number cleanup:YES];
    }
    
    [clonedNumbers_ removeAllObjects];    
    
    [self resetInputScales];
    [self resetInputColors];
    [self logAppState];
    
    [positionWithValueCarry removeAllObjects];
    [positionWithValueSum removeAllObjects];
    
    NSLog(@"AFTER Cloned Count: %d", [clonedNumbers_ count]);
}


-(void) ccTouchMoved:(UITouch *)touch withEvent:(UIEvent *)event
{   
    CGPoint touchLocation=[self locationFromTouch:touch];
    if(activeSprite_ != nil){
        if([clonedNumbers_ containsObject:activeSprite_]){
            [((CCSprite*)activeSprite_) setPosition:touchLocation];
            CCSprite *currentlyActiveSprite = ((CCSprite *)activeSprite_);        
            CGRect currentlyActiveRect   = [currentlyActiveSprite boundingBox];
            
            CCLabelTTF * child = ((CCLabelTTF *)[currentlyActiveSprite children].lastObject);
            
            NSString *currentlyActiveLabel = [child string];
            
            // sumPositions        
            NSEnumerator *s = [sumPositions objectEnumerator];
            id sumObject;
            while (sumObject = [s nextObject]) {
                CCSprite *sumsPositionSprite = ((CCSprite *)sumObject);
                CGRect targetSumsPositionRect  = [sumsPositionSprite boundingBox];
                CCLabelTTF * sumPositionChild = ((CCLabelTTF *)[sumsPositionSprite children].lastObject);

                if (CGRectContainsRect(currentlyActiveRect, targetSumsPositionRect)){
                    NSLog(@"touch moved before calling clear place holder preview.");
                    [self _clearPlaceHoldersPreview];
                    [sumPositionChild setColor:ccYELLOW];
                    [sumPositionChild setString:currentlyActiveLabel];
                    hoveredOverSprite_ = sumsPositionSprite;
                    if(hoveredOverSprite_ == previousHoveredOverSprite_ ){
                    }else {
                        previousHoveredOverSprite_ = hoveredOverSprite_;
                        [self performSelector: @selector(_playHoverOverSound)]; 
                    }
                    return;
                }
                else if(previousHoveredOverSprite_ != nil){ 
                    CGRect previousHoveredPositionRect = ((CCSprite*)previousHoveredOverSprite_).boundingBox;
                    if (CGRectIntersectsRect(currentlyActiveRect, previousHoveredPositionRect)){
                        hoveredOverSprite_ = previousHoveredOverSprite_;
                    }else{
                        [sumPositionChild setString:@"  "];
                        previousHoveredOverSprite_ = nil;
                        return;
                    }
                }
                else {
                    [sumPositionChild setString:@"  "];
                    previousHoveredOverSprite_ = nil;
                }
            }
            
            NSEnumerator *e = [carryPositions objectEnumerator];
            id carryObject;
            
            while (carryObject = [e nextObject]) {
                CCSprite *carryPositionSprite = ((CCSprite *)carryObject);
                CGRect targetCarryPositionsRect  = [carryPositionSprite boundingBox];
                CCLabelTTF * carryPositionChild = ((CCLabelTTF *)[carryPositionSprite children].lastObject);

                if (CGRectContainsRect(currentlyActiveRect, targetCarryPositionsRect)){
                    [self _clearPlaceHoldersPreview];
                    [carryPositionChild setColor:ccYELLOW];
                    [carryPositionChild setString:currentlyActiveLabel];
                    hoveredOverSprite_ = carryPositionSprite;
                    if(hoveredOverSprite_ == previousHoveredOverSprite_ ){
                    }else {
                        previousHoveredOverSprite_ = hoveredOverSprite_;
                        [self performSelector: @selector(_playHoverOverSound)]; 
                    }
                    return;
                }
                else if(previousHoveredOverSprite_ != nil){ 
                    CGRect previousHoveredPositionRect = ((CCSprite*)previousHoveredOverSprite_).boundingBox;
                    if (CGRectIntersectsRect(currentlyActiveRect, previousHoveredPositionRect)){
                        hoveredOverSprite_ = previousHoveredOverSprite_;
                    }else{
                        [carryPositionChild setString:@"  "];
                        previousHoveredOverSprite_ = nil;
                        return;
                    }
                }
                else{
                    [carryPositionChild setString:@"  "];
                    hoveredOverSprite_ = nil;
                }
            }
        }
    }
}

-(void)_playHoverOverSound{
    [[SimpleAudioEngine sharedEngine] playEffect:SOUND_FX_HOVER];
}

-(void)ccTouchEnded:(UITouch *)touch withEvent:(UIEvent *)event {
    NSMutableArray *discardItems = [NSMutableArray array];
    GameSessionManager *gm = [GameSessionManager sharedGameSessionManagerInstance];
    
    if(currentlySelectedNumber_ != nil){
        currentlySelectedNumber_.scaleX = 1;
        currentlySelectedNumber_.scaleY = 1;
        currentlySelectedNumber_ = nil;
    }

    CGPoint touchLocation = [self locationFromTouch:touch];
    if(activeSprite_ != nil){
        CCSprite *currentlyActiveSprite = ((CCSprite*)activeSprite_);
        currentlyActiveSprite.position = touchLocation;
        currentlyActiveSprite.scaleX = 1;
        currentlyActiveSprite.scaleY = 1;
    }
    
    BOOL isValidPlacement = NO;
    
    NSEnumerator *e = [clonedNumbers_ objectEnumerator];
    id numberMenuItem;
    
    while (numberMenuItem = [e nextObject]) {
        // Check for Collision in the Carry and SUM placeholders
        CCSprite *clonedSprite = ((CCSprite *)numberMenuItem);

        if(hoveredOverSprite_ != nil){
            CCSprite *currentlyHoveredOver = ((CCSprite*)hoveredOverSprite_);
            
            CGRect clonedRect   = [clonedSprite boundingBox];
            CGRect currentlyHoveredOverRect  = [currentlyHoveredOver boundingBox];
            
            if (CGRectIntersectsRect(clonedRect, currentlyHoveredOverRect)){
                clonedSprite.position = ccp(currentlyHoveredOver.position.x, currentlyHoveredOver.position.y);                
                [[SimpleAudioEngine sharedEngine] playEffect:SOUND_FX_DROP];
                clonedSprite.scaleX = 1;
                clonedSprite.scaleY = 1;
                
                BOOL isCarry = NO;
                if([carryPositions containsObject: hoveredOverSprite_]){
                    isCarry = YES;
                }
                
                isValidPlacement = YES;
                
                CCLabelTTF * userDataTTF = ((CCLabelTTF *) [currentlyHoveredOver children].lastObject);
                
                NSString *key = ((NSString*)[userDataTTF userData]);
                NSLog(@"Touch End Key: %@", key);
                if(isCarry){
                    id value = [positionWithValueCarry objectForKey:key];
                        if(value == clonedSprite){
                            [self removeChild:value cleanup:YES];
                            [discardItems addObject:value];
                            continue;
                    }
                    [positionWithValueCarry setValue:clonedSprite forKey:key];
                }
                else{
                    id value = [positionWithValueSum objectForKey:key];
                        if(value == clonedSprite){
                            [self removeChild:value cleanup:YES];
                            [discardItems addObject:value];
                            continue;
                        }
                    [positionWithValueSum setValue: clonedSprite forKey:key];
                }
            }
        }
    }
    
    if(!isValidPlacement){
        if(activeSprite_ != nil){
            
            // should be discarded.
            CCSprite *item = ((CCSprite*)activeSprite_);
            [self performSelector:@selector(_playRetractSound) ];
            // animate
            CGPoint _targetPosition = [self getNumberMenuItemCoordinates:item];
            CCAction *moveAction = [CCMoveBy actionWithDuration:0.4f position:[item convertToNodeSpaceAR: _targetPosition ]];
            [item runAction:moveAction];
            
            // add to discard queue to remove from clonedNumbers array.
            [discardItems addObject:item];
            
            // remove discarded number from positionWithValueSum Dict
            for(id key in positionWithValueSum) {
                id value = [positionWithValueSum objectForKey:key];
                
                if (value == activeSprite_)
                {
                    [positionWithValueSum setValue:zero_ forKey:key];
                    break;
                }
            }

            // remove discarded number from positionWithValueCarry Dict
            for(id key in positionWithValueCarry) {
                id value = [positionWithValueCarry objectForKey:key];
                
                if (value == activeSprite_)
                {
                    [positionWithValueCarry setValue:zero_ forKey:key];
                    break;
                }
            }
            
            // remove child after the animation happens - otherwise, just disappears.  
            //  Schedule the removeChild after the animation duration.
            [self performSelector:@selector(removeChild:cleanup:) withObject:item afterDelay:0.5f];
        }
    }
    [clonedNumbers_ removeObjectsInArray:discardItems];
    
    [self resetInputColors];
    [self resetInputScales];
    
    NSLog(@"touch **ended** before calling clear place holder preview.");

    [self _clearPlaceHoldersPreview];
    [self logAppState];
    
    // reset activeSprite to nil
    hoveredOverSprite_ = nil;
    activeSprite_ = nil;
    previousHoveredOverSprite_ = nil;
    
    if (gm.hintState == CONFIG_SESSION_HINT_MODE_ON && gm.mode == CONFIG_SESSION_MODE_PRACTICE)
    {
        [self performSelector:@selector(giveHint:)];
    }
}

-(void)_playRetractSound{
    [[SimpleAudioEngine sharedEngine] playEffect:SOUND_FX_RETRACT];
}

-(void) registerWithTouchDispatcher
{
	[[CCTouchDispatcher sharedDispatcher] addTargetedDelegate:self priority:0 swallowsTouches:YES];
}

-(CGPoint) locationFromTouch:(UITouch*)touch
{
    CGPoint touchLocation = [touch locationInView: [touch view]]; 
    return [[CCDirector sharedDirector] convertToGL:touchLocation];
}

-(CGPoint) locationFromTouches:(NSSet*)touches
{ 
    return [self locationFromTouch:[touches anyObject]];   
}

-(bool) isTouchForMe:(CGPoint)touchLocation:(CCNode *)tagNode
{
	return CGRectContainsPoint([tagNode boundingBox], touchLocation);
}


// on "dealloc" you need to release all your retained objects
- (void) dealloc
{
	// in case you have something to dealloc, do it in this method
	// in this particular example nothing needs to be released.
	// cocos2d will automatically release all the children (Label)
	
	// don't forget to call "super dealloc"
	[super dealloc];
}
@end
