//
//  SceneFactory.h
//  Kinopi
//  Created by  Dennis Park (dennis.park@gmail.com) on 8/22/12.
//              Phill Pafford (phill.pafford@gmail.com)
//  Copyright Reliable Source LLC 2012. All rights reserved.
//

#import <Foundation/Foundation.h>
#import "cocos2d.h"

@interface SceneFactory : NSObject{

}
+(SceneFactory *)sharedSceneFactoryInstance;
-(CCScene*)getScene:(NSString*)sceneId;
@end
