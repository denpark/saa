//
//  GameSessionManager.m
//  saa
//
//  Created by Dennis Park on 8/7/12.
//  Copyright Reliable Source, LLC. 2012. All rights reserved.
//

#import "GameSessionManager.h"

@implementation GameSessionManager
@synthesize currentProblem      = _currentProblem;
@synthesize correct             = _correct;
@synthesize incorrect           = _incorrect;
@synthesize score               = _score;
@synthesize mode                = _mode;
@synthesize hintState           = _hintState;

static GameSessionManager *_sharedGameSessionManagerInstance = nil;


+(GameSessionManager *)sharedGameSessionManagerInstance{
    @synchronized([GameSessionManager class])
    {
        if(!_sharedGameSessionManagerInstance)
            [[self alloc] init];
        return _sharedGameSessionManagerInstance;
    }
    return nil;
}

+(id)alloc
{
    @synchronized([GameSessionManager class])
    {
        _sharedGameSessionManagerInstance = [super alloc];
        return _sharedGameSessionManagerInstance;
    }
    return nil;
}

-(int)getScore
{
    NSLog(@"config session total problems:  %i", CONFIG_SESSION_TOTAL_PROBLEMS);
    float percentage = self.correct / (float)CONFIG_SESSION_TOTAL_PROBLEMS;
    NSLog(@"percentage:  %f", percentage);
    if(percentage >= .8) {
        return 3;
    }else if(percentage >=.4){
        return 2;
    }else if(percentage>=.1){
        return 1;
    }
    return 0;
    
}

-(void)resetSession
{
    _currentProblem = 0;
    _score = 0;
    _incorrect = 0;
    _correct = 0;
}

-(id)init{
    self = [super init];
    if(self != nil){
        [self resetSession];
        self.hintState = CONFIG_SESSION_HINT_MODE_ON;
    }
    return self;
}


// on "dealloc" you need to release all your retained objects
- (void) dealloc
{
    [_sharedGameSessionManagerInstance release];
    _sharedGameSessionManagerInstance = nil;
	[super dealloc];
}@end
