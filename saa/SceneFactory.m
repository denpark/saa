//
//  SceneFactory.m
//  Kinopi
//
//  Created by  Dennis Park (dennis.park@gmail.com) on 8/22/12.
//              Phill Pafford (phill.pafford@gmail.com)
//  Copyright Reliable Source LLC 2012. All rights reserved.
//

#import "SceneFactory.h"
#import "LayerGameAddition.h"
#import "LayerMenuIntro.h"
#import "LayerLevelComplete.h"

@implementation SceneFactory
static SceneFactory *_sharedSceneFactoryInstance = nil;


+(SceneFactory *)sharedSceneFactoryInstance{
    @synchronized([SceneFactory class])
    {
        if(!_sharedSceneFactoryInstance)
            [[self alloc] init];
        return _sharedSceneFactoryInstance;
    }
    return nil;
}

+(id)alloc
{
    @synchronized([SceneFactory class])
    {
        _sharedSceneFactoryInstance = [super alloc];
        return _sharedSceneFactoryInstance;
    }
    return nil;
}


-(id)init{
    self = [super init];
    if(self != nil){
        // nothing
    }
    return self;
}

-(CCScene*)getScene:(NSString*)sceneId{
    NSLog(@"SceneFactory:getScene: %@", sceneId );
    // 'scene' is an autorelease object.
    CCScene *scene = [CCScene node];

    CCLayer * layer = nil;

    if([sceneId isEqualToString:@"sceneGameAddition"]) {
        layer = [LayerGameAddition node];
    } else if([sceneId isEqualToString:@"sceneMenuOption"]) {
        layer = [LayerMenuIntro node];
    } else if([sceneId isEqualToString:@"sceneLevelComplete"]){
        layer = [LayerLevelComplete node];
    } else {
        layer = [LayerMenuIntro node];
    }
    
    // add layer as a child to scene
    [scene addChild: layer];
    
    // return the scene
    return scene;    
}



// on "dealloc" you need to release all your retained objects
- (void) dealloc
{
    [_sharedSceneFactoryInstance release];
    _sharedSceneFactoryInstance = nil;
	[super dealloc];
}
@end
