//
//  LayerMenuIntro.h
//  Kinopi
//
//  Created by  Dennis Park (dennis.park@gmail.com) on 8/22/12.
//              Phill Pafford (phill.pafford@gmail.com)
//  Copyright Reliable Source LLC 2012. All rights reserved.
//

// When you import this file, you import all the cocos2d classes
#import "cocos2d.h"
#import "SceneFactory.h"
#import "GameConfig.h"
#import "GameSessionManager.h"

// HelloWorldLayer
@interface LayerMenuIntro : CCLayer
{
//    CCMenuItemFont *introItem;
	CCMenu *menu;
}

@end