//
//  LayerLevelComplete.h
//  Kinopi: Adding
//
//  Created by  Dennis Park (dennis.park@gmail.com) on 07/04/13.
//              Phill Pafford (phill.pafford@gmail.com)
//  Copyright Reliable Source LLC 2012. All rights reserved.
//

#import <Foundation/Foundation.h>
#import "cocos2d.h"
#import "SceneFactory.h"
#import "GameSessionManager.h"
#import "GameConfig.h"

@interface LayerLevelComplete : CCLayer {
    CCMenu *menu;

}
@end
