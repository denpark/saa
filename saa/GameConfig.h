//
//  GameConfig.h
//  saa
//
//  Created by Dennis Park on 5/22/12.
//  Copyright __MyCompanyName__ 2012. All rights reserved.
//

#ifndef __GAME_CONFIG_H
#define __GAME_CONFIG_H

//
// Supported Autorotations:
//		None,
//		UIViewController,
//		CCDirector
//
#define kGameAutorotationNone 0
#define kGameAutorotationCCDirector 1
#define kGameAutorotationUIViewController 2

#define DIM_FONT 64
#define PADDING_VERTICAL 5
#define PADDING_HORIZONTAL 17
#define VIZ_FX_SCALE_X 1.71
#define VIZ_FX_SCALE_Y 1.71

#define COLUMN_IS_CORRECT -99;

#define LABEL_NEXT @"Next"
#define LABEL_HOME @"Home"
#define LABEL_SUBMIT @"Submit"
#define LABEL_RESET @"Reset"
#define LABEL_HINT_ON @"Hint: On"
#define LABEL_HINT_OFF @"Hint: Off"
#define LABEL_SCORE @"Score: "
#define LABEL_NEXT_LEVEL @"Next Level"

#define SOUND_FX_PRESS @"pop.mp3"    // from http://www.freesfx.co.uk/info/eula/
#define SOUND_FX_DROP @"drop.wav"    // from http://soundbible.com/1126-Water-Drop.html
#define SOUND_FX_HOVER @"click.wav"  // from http://www.jewelbeat.com/free/free-sound-effects-1.htm
#define SOUND_FX_RETRACT @"reel.aiff" // from http://soundfxnow.com/sound-fx/reeling-fishing-reel/

#define IMAGE_BLANK_TILE_67_X_67 @"tile1_blank_67.png" // http://www.vickiwenderlich.com/2012/12/free-game-art-letter-tiles/

#define CONFIG_SESSION_TOTAL_PROBLEMS 10
#define CONFIG_SESSION_MODE_PRACTICE 20
#define CONFIG_SESSION_MODE_QUIZ 30

#define CONFIG_SESSION_HINT_MODE_ON 1
#define CONFIG_SESSION_HINT_MODE_OFF 0

#define CONFIG_SESSION_EQUATION_NUMBER_COUNT_AUGEND 2   // Should not exceed 4 (thousandths place)
#define CONFIG_SESSION_EQUATION_NUMBER_COUNT_ADDEND 1   // Should not exceed 4 (thousandths place)

//
// Define here the type of autorotation that you want for your game
//

// 3rd generation and newer devices: Rotate using UIViewController. Rotation should be supported on iPad apps.
// TIP:
// To improve the performance, you should set this value to "kGameAutorotationNone" or "kGameAutorotationCCDirector"
#if defined(__ARM_NEON__) || TARGET_IPHONE_SIMULATOR
#define GAME_AUTOROTATION kGameAutorotationUIViewController

// ARMv6 (1st and 2nd generation devices): Don't rotate. It is very expensive
#elif __arm__
#define GAME_AUTOROTATION kGameAutorotationNone


// Ignore this value on Mac
#elif defined(__MAC_OS_X_VERSION_MAX_ALLOWED)

#else
#error(unknown architecture)
#endif

#endif // __GAME_CONFIG_H

