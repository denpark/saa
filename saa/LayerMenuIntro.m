//
//  LayerMenuIntro.m
//  Kinopi
//
//  Created by  Dennis Park (dennis.park@gmail.com) on 8/22/12.
//              Phill Pafford (phill.pafford@gmail.com)
//  Copyright Reliable Source LLC 2012. All rights reserved. 
//

#import "LayerMenuIntro.h"

@implementation LayerMenuIntro

-(id) init {
    if( (self=[super init])) {
        
        //Set font size/name
        CCSprite *bg = [CCSprite spriteWithFile:@"blank.png"];
        bg.position = ccp(512,768/2);
        [bg setTextureRect:CGRectMake(0,0,1024,768)];
        [bg setColor:ccc3(arc4random()%150,arc4random()%150,arc4random()%150)];
        [self addChild:bg];
        
        //Options Label
        CCMenuItemFont *introItemPractice = [CCMenuItemFont itemFromString:@"Practice" target:self selector:@selector(practice:)];
        CCMenuItemFont *introItemQuiz = [CCMenuItemFont itemFromString:@"Take Quiz" target:self selector:@selector(quiz:)];
        CCMenuItemFont *introItemOptions = [CCMenuItemFont itemFromString:@"Options" target:self selector:@selector(options:)];
        
        //Add menu items
        menu = [CCMenu menuWithItems: introItemPractice, introItemQuiz, introItemOptions, nil];
        [menu alignItemsVertically];
        [self addChild:menu];
        return self;
    }
}

-(void)back:(id)sender{
    NSLog(@"back selector called");
    [[CCDirector sharedDirector] popScene];
}
-(void)difficultyToggle:(id)sender{
    NSLog(@"difficulty selector called");
}

-(void) soundToggle:(id)sender
{
    NSLog(@"sound toggle selector called");
}
-(void) reAlign:(id)sender{
    NSLog(@"realign selector called");
}

-(void) options:(id)sender {
    CCScene * sceneMenuOption = [[SceneFactory sharedSceneFactoryInstance] getScene:@"sceneMenuOption"];
	[[CCDirector sharedDirector] pushScene:sceneMenuOption];
}
-(void) practice:(id)sender {
    [GameSessionManager sharedGameSessionManagerInstance].mode = CONFIG_SESSION_MODE_PRACTICE;
    CCScene * scene = [[SceneFactory sharedSceneFactoryInstance] getScene:@"sceneGameAddition"];
        
	[[CCDirector sharedDirector] pushScene:scene];
}
-(void) quiz:(id)sender {
    [GameSessionManager sharedGameSessionManagerInstance].mode = CONFIG_SESSION_MODE_QUIZ;
    CCScene * scene = [[SceneFactory sharedSceneFactoryInstance] getScene:@"sceneGameAddition"];
    
	[[CCDirector sharedDirector] pushScene:scene];
}

-(void)buttonTouched:(id)sender{
    NSLog(@"button touched selector called");
}

// on "dealloc" you need to release all your retained objects
- (void) dealloc
{
	// in case you have something to dealloc, do it in this method
	// in this particular example nothing needs to be released.
	// cocos2d will automatically release all the children (Label)
	
	// don't forget to call "super dealloc"
	[super dealloc];
}

@end



