//
//  LayerLevelComplete.m
//  Kinopi: Adding
//
//  Created by  Dennis Park (dennis.park@gmail.com) on 07/04/13.
//              Phill Pafford (phill.pafford@gmail.com)
//  Copyright Reliable Source LLC 2013. All rights reserved.
//

#import "LayerLevelComplete.h"
#import "LayerGameAddition.h"

@implementation LayerLevelComplete

-(id) init {
    if( (self=[super init])) {
        CGSize size = [[CCDirector sharedDirector] winSize];
        
        float screenHeight = size.height;
        float screenWidth  = size.width;
        
        CCSprite *bg = [CCSprite spriteWithFile:@"blank.png"];
        bg.position = ccp(screenWidth/2,screenHeight/2);
        [bg setTextureRect:CGRectMake(0,0,screenWidth,screenHeight)];
        [bg setColor:ccc3(arc4random()%150,arc4random()%150,arc4random()%150)];
        [self addChild:bg];
        
        //Options Label
        CCMenuItemFont *nextLevel = [CCMenuItemFont itemFromString:LABEL_NEXT_LEVEL target:self selector:@selector(playAgain:)];
        CCMenuItemFont *home = [CCMenuItemFont itemFromString:LABEL_HOME target:self selector:@selector(home:)];
        
        //Add menu items
        menu = [CCMenu menuWithItems: nextLevel, home, nil];
        [menu alignItemsVertically];
        [self addChild:menu];
        GameSessionManager *gm = [GameSessionManager sharedGameSessionManagerInstance];

        short score = gm.getScore;
        NSString * accolade = [NSString stringWithFormat:@"Great Job! You have earned %d star(s)!! ", score];
        CCLabelTTF *userScore = [CCLabelTTF labelWithString:accolade fontName:@"Marker Felt" fontSize:DIM_FONT];
        CGFloat scoreHeight = [userScore boundingBox].size.height;

        userScore.position = ccp((screenWidth / 2), (screenHeight) - ((scoreHeight / 2) + DIM_FONT));

        [self addChild:userScore];
        
        [gm resetSession];
        return self;
    }
    return nil;
}

-(void) home:(id)sender {
    CCDirector * director = [CCDirector sharedDirector];
    [director popScene];
    [director popScene];
    
}

-(void) playAgain:(id)sender {    
    // No need to create new scene and push onto director.  Just reset the
    //     session and pop this scene off.
    CCDirector * director = [CCDirector sharedDirector];
    [director popScene];
}
@end
