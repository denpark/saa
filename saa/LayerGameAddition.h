//
//  LayerGameAddition.h
//  Kinopi
//
//  Created by  Dennis Park (dennis.park@gmail.com) on 5/22/12.
//              Phill Pafford (phill.pafford@gmail.com)
//  Copyright Reliable Source LLC 2012. All rights reserved.
//


// When you import this file, you import all the cocos2d classes
#import <Foundation/Foundation.h>
#import "cocos2d.h"
#import "GameConfig.h"

#define LAYOUT_RHS 0
#define LAYOUT_LHS 1

#define LAYOUT_ONE_COL 1
#define LAYOUT_TWO_COL 2
#define LAYOUT_FIVE_COL 5
#define LAYOUT_TEN_COL 10

#define DISPLAY_CORRECT 5000
#define DISPLAY_TOTAL 5100

#define HINT_TOGGLE_STATE 5200

@interface LayerGameAddition : CCLayer
{
    // START REFACTOR
    NSNumber *zero_;
    NSNumber *one_;
    NSNumber *two_;
    NSNumber *three_;
    NSNumber *four_;
    NSNumber *five_;
    NSNumber *six_;
    NSNumber *seven_;
    NSNumber *eight_;
    NSNumber *nine_;
    
    // Push the NSNumbers above into this array to easily determine the number
    // by finding it by int / index in the array
    NSArray *numberValues_;
    
    // Place Systems
    NSArray *placeSystemValues;
    
    // Position with Value of each part of the addition problem
    NSMutableDictionary *positionWithValueCarry;
    NSMutableDictionary *positionWithValueAugend;
    NSMutableDictionary *positionWithValueAddend;
    NSMutableDictionary *positionWithValueSum;
    
    // We aso need to keep track of collision
    
    id activeSprite_;                 
    id hoveredOverSprite_;
    id previousHoveredOverSprite_;
    CCSprite *currentlySelectedNumber_;


    int numberMenuTotalSpriteCount;   
    int equationNumberCountAugend;    
    int equationNumberCountAddend;    
    int equationNumberTotalCount;     
    
    
    // END REFACTOR
    
    //                     TTHTO
    //                     ehuen
    //                     nonne
    //                     tudss
    //                     hsr
    //                     oae
    //                     und
    //                     sds
    //                     as
    //                     n
    //                     d
    //                     s
    //
    //                     |||||
    //                     vvvvv  <-- Place System Values  -- Ones, Tens, Hundreds, etc...
    //                     xxxx   <-- Carry                -- transfer units of tens from one column to the next
    //                       999  <-- Argend               -- Top number in the equation, Alternative Addend
    //                       123  <-- Addend               -- First Added, Additional Addend will stack under this one
    // Math Operator -->  +-----  <-- Line                 -- Separates the Equation from the Answer
    //                      xxxx  <-- Sum                  -- The Answer to the Equation
    //                   |__|__|  <-- Place System Periods -- Units, Thousands, Millions
    //                     ^  ^
    //                     |  |
    //                     T  U
    //                     h  n
    //                     o  i
    //                     u  t
    //                     s  s
    //                     a
    //                     n
    //                     d
    //                     s
    //
    
    
    // Numbers in the Equation Container: This should be a NSMutableDictionary to hold the "Numbers in the Equation" Position ( See below )
    
    // Numbers in the Equation: (Augend, Addend, Carry, Sum) NSMutableDictionary since we need to track the Place System Values
    
    // Place System Values: Ones, Tens, Hundreds. NSArray as these should never change
    
    // Place System Periods: Units, Thousands, Millions. NSArray as these should never change
    
    // 
    
    
    
    
    
    /**
     * Things we need to track
     * 
     * 01- number values 0 - 9
     * 02- numbers in the equation
     * 		a- Augend
     * 		b- Addend
     * 		c- Carry
     * 		d- Sum
     * 03- position of Number Menu Sprites ( 0 - 9 )
     * 04- position of Carry Placeholder Sprites
     * 05- position of Augend Placeholder Sprites
     * 06- position of Addend Placeholder Sprites
     * 07- position of Sum Placeholder Sprites
     * 08- place system values ( http://cnx.org/content/m26899/latest/ )
     * 		a- 'ones'
     * 		b- 'tens'
     * 		c- 'hundreds'
     * 09- place system periods ( http://cnx.org/content/m26899/latest/ ) *** NOTE: THIS IS A MAYBE ***
     * 		a- 'units'
     * 		b- 'thousands'
     * 		c- 'millions'
     * 10- place system periods for Placeholder Sprites
     * 		a- Carry
     * 		b- Augend
     * 		c- Addend
     * 		d- Sum
     * 11- place system values for Placeholder Sprites
     * 		a- Carry
     * 		b- Augend
     * 		c- Addend
     * 		d- Sum
     * 12- Number of Items
     * 13- non-draggable items
     * 		a- Operator Symbol
     * 		b- Reset Button
     * 		c- Solve / Validate / Check Equation Button ( Yet to be implemented )
     * 14- collision place system period and value on placeholders
     * 		a- Carry
     * 		b- Sum
     * 
     * Addition Features we could track
     * 15- Active Column
     * 		a- Carry, Augend and Addend for a given place system period and value
     * 16- Sum of Active Column
     * 		a- Results of bullet 14 "Active Column" used to push ( if needed ) the next carry position value
     * 			Example: If the Sum of Carry, Augend and Addend for a given place system 'unit' and 'ones' is 27 ( 9 + 9 + 9 = 27 )
     * 					 then the 2 is pushed to the next Carry position and the 7 is pushed to the Sum 'ones' position
     */
    
    // when selectedItem = -1, it means nothing is selected.
  //  int selectedItem;
   // int totalItems;
//    int numberMenuSpriteCount; // @TODO: Should this just be a static 10? Remove the variable?
    int numberMenulabelPlaceholder;
//    int operatorPlaceholder;
//    int resetPlaceholder;
    int augendNumberCount;
    int addendNumberCount;
    int totalNumberForEquation;
    
    // Carry Sprite Position Array
    NSMutableArray *carryPositions;
    
    // SUM Sprite Posiiton Array
    NSMutableArray *sumPositions;
    
    // Bucket of each part of the addition problem
    NSMutableArray *bucketAugend;
    NSMutableArray *bucketAddend;

    // Equation Line info
    float equationLine_y_;
    float equationLine_x_start_;
    float equationLine_x_end_;
    
    // Screen dimensions
    float screenWidth_;
    float screenHeight_;
    
    // Cloned numbers array
    NSMutableArray *clonedNumbers_;
    NSMutableArray *numbersMenuItems_;
    
    float screenScaleFactor_x_;
    float screenScaleFactor_y_;

    uint fontSize_;
    uint lineWidth_;
    uint numberMenuLayoutOrientation_;
    uint numberMenuLayoutColumns_;
    
    CGRect inputBackgroundDim_;    
}

-(CGPoint) locationFromTouch:(UITouch*)touch;
-(CGPoint) locationFromTouches:(NSSet*)touches;
-(BOOL) ccTouchBegan:(UITouch*)touch withEvent:(UIEvent *)event;
-(void) ccTouchMoved:(UITouch *)touch withEvent:(UIEvent *)event;
-(void) ccTouchEnded:(UITouch *)touch withEvent:(UIEvent *)event;
-(bool) isTouchForMe:(CGPoint)touchLocation:(CCNode *)tagNode;

@property (nonatomic, retain) NSMutableArray *carryPositions;
@property (nonatomic, retain) NSMutableArray *sumPositions;

@property (nonatomic, retain) NSMutableArray *bucketAugend;
@property (nonatomic, retain) NSMutableArray *bucketAddend;


//START REFACTOR
@property (nonatomic, retain) NSNumber *zero;
@property (nonatomic, retain) NSNumber *one;
@property (nonatomic, retain) NSNumber *two;
@property (nonatomic, retain) NSNumber *three;
@property (nonatomic, retain) NSNumber *four;
@property (nonatomic, retain) NSNumber *five;
@property (nonatomic, retain) NSNumber *six;
@property (nonatomic, retain) NSNumber *seven;
@property (nonatomic, retain) NSNumber *eight;
@property (nonatomic, retain) NSNumber *nine;

@property (nonatomic, retain) NSArray *placeSystemValues;
//@property (nonatomic, retain) NSArray *reversedPlaceSystemValues;
//@property (nonatomic, retain) NSArray *placeSystemPeriods;

@property (nonatomic, retain) NSMutableDictionary *positionWithValueCarry;
@property (nonatomic, retain) NSMutableDictionary *positionWithValueAugend;
@property (nonatomic, retain) NSMutableDictionary *positionWithValueAddend;
@property (nonatomic, retain) NSMutableDictionary *positionWithValueSum;

@property (nonatomic, retain) NSArray *numberValues;

@property (nonatomic, retain) id activeSprite;
@property (nonatomic) int numberMenuTotalSpriteCount;
@property (nonatomic) int equationNumberCountAugend;
@property (nonatomic) int equationNumberCountAddend;
@property (nonatomic) int equationNumberTotalCount;
@property (nonatomic) float equationLine_y;
@property (nonatomic) float equationLine_x_start;
@property (nonatomic) float equationLine_x_end;
@property (nonatomic) float screenWidth;
@property (nonatomic) float screenHeight;
@property (nonatomic) CGRect inputBackgroundDim;
@end

